unbind C-b
set -g prefix `
set -g prefix2 'C-\' # Control+' as a second prefix.
bind ` send-prefix
bind a source-file ~/.tmux.conf \; display " ~/.tmux.conf sourced."

set -s escape-time 0
set -g allow-rename on
set -g set-titles on
set -g base-index 1
set -g renumber-windows 1
set -g set-clipboard off
setw -g aggressive-resize on
setw -g pane-base-index 1

# (defun color6 (r g b) (format "colour%d" (+ 16 (* r 36) (* g 6) b)))
# (defun grey (v) (format "colour%d" (+ 232 v))) ; v: 0..23
# (color6 3 1 1) "colour131"
set -g default-terminal "xterm-256color"
set -g message-style bg=default,fg=colour131
set -g message-command-style bg=colour3,fg=white
set -g status-position bottom
set -g status-style bg=default,fg=colour244
set -g status-justify left
set -g status-left \
    '(#{?client_prefix,#[fg=colour3],#[fg=colour1]}#S#[fg=default]) '
set -g status-left-length 40
set -g status-right ' #{history_size} @#{host_short} '
setw -g mode-style bg=colour131,fg=colour15,bold
setw -g window-status-style bg=default,fg=colour131
setw -g window-status-format '#I#F #W '
setw -g window-status-separator ' '
setw -g window-status-current-style fg=default,bold,reverse
setw -g window-status-current-format '#I#[fg=default,noreverse]#F #W '
setw -g window-status-bell-style bg=colour1,fg=colour255,bold
setw -g clock-mode-colour colour131

# The mouse mode is currently broken for text selection.
# I tried the following set of unbinds, and they seem to disable mouse-drag
# selection altogether, not falling back to the system's own mouse-drag.
# unbind-key -T copy-mode MouseDrag1Pane
# unbind-key -T copy-mode MouseDragEnd1Pane
# unbind-key -T copy-mode-vi MouseDrag1Pane
# unbind-key -T copy-mode-vi MouseDragEnd1Pane
# unbind-key -T root MouseDrag1Pane
# unbind-key -T root MouseDrag1Border
set -g mouse off

#!/bin/bash

# Example script:
# #!/bin/bash
#
# source "$HOME/.local/etc/scripty"
#
# docs[.verb]="ARGS..."
# .verb() { ... }
#
# main "$@"

# Functions starting with a dot are "exposed" as verbs.
main() {
  if [[ "$#" = 0 ]] ; then
    .help
    return
  fi
  ".$@"
}

declare -A docs=()
docs[.help]="<No or one arg>
  With no args, prints list of commands.
  With one arg, considered the verb, prints the docstring for that verb."
.help() {
  local program="${0##*/}"
  if [[ "$#" = 0 ]] ; then
    echo "Usage:"
    # Exposed verb functions must start with a dot character, e.g., .buildall(),
    # .build-all(), etc.
    declare -F | while read f ; do  # f looks like "declare -f Functionname"
      if [[ "$f" =~ ^declare\ -f\ [.] ]] ; then
        local verb="${f#declare -f .}"  # Strip the prefix.
        local docstring="${docs[.$verb]}"
        echo "\$ ${program} ${verb} ${docstring}"
      fi
    done
  elif [[ "$#" = 1 ]] ; then
    local verb="$1"
    echo "\$ ${program} ${verb} ${docs[.$verb]}"
  fi
}

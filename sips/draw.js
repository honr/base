function draw() {
  const ctx = new Canvas(512, 512);
  for (var i = 0; i < 6; i++) {
    for (var j = 0; j < 6; j++) {
      ctx.fillStyle = 'rgb(' + Math.floor(255 - 42.5 * i) + ', ' +
        Math.floor(255 - 42.5 * j) + ', 0)';
      // ctx.fillRect(j * 25, i * 25, 25, 25);

      ctx.strokeStyle = 'rgb(0, ' + Math.floor(255 - 42.5 * i) + ', ' +
        Math.floor(255 - 42.5 * j) + ')';
      ctx.beginPath();
      ctx.arc(12.5 + j * 25, 12.5 + i * 25, 10, 0, Math.PI * 2, true);
      ctx.stroke();

    }
  }

  // for (var i = 0; i < 10; i++) {
  //   ctx.lineWidth = 1 + i;
  //   ctx.beginPath();
  //   ctx.moveTo(5 + i * 14, 5);
  //   ctx.lineTo(5 + i * 14, 140);
  //   ctx.stroke();
  // }

  ctx.fillStyle = '#eee';

  ctx.fillRect(4, 4, 502, 502);

  ctx.strokeStyle = '#888';
  ctx.fillStyle = '#222';
  ctx.strokeRect(2, 2, 506, 506);
  ctx.font = '18pt Merriweather';

  ctx.shadowOffsetX = 0;
  ctx.shadowOffsetY = -1;
  ctx.shadowBlur = 3;
  ctx.shadowColor = 'rgba(0,0,0, 0.3)';

  ctx.fillText('Hello World', 4, 22);


  const filename = './a.png';
  const output = new Output(ctx, filename);
  output.addToQueue();
}

print(sips.configuration.model);

draw();

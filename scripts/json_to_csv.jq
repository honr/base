(.[0] | keys_unsorted) as $keys
| ($keys | map(. as $k | $k)), # Ensure keys are quoted
  ( .[]
    | [.[$keys[]]] # Extract values based on header order
    | map(. as $v | $v))
| @csv

BEGIN {
  FS = ","
  OFS = ""
}

NR == 1 {
  # Store header row as field names
  for (i = 1; i <= NF; i++) {
    gsub(/"/, "", $i)  # Remove quotes if present
    field[i] = $i
  }
}

NR > 1 {
  # Output JSON object for each row
  printf("{\n")
  for (i = 1; i <= NF; i++) {
    gsub(/"/, "", $i)  # Remove quotes
    if (i > 1) printf(",\n")  # Add comma after the first field
    printf("  \"%s\": \"%s\"", field[i], $i) # Format key-value pair
  }
  printf("\n}\n")
}

// String:

String.prototype.toCamelCase = function(pattern) {
  if (!pattern) {
    pattern = /[-_/.]/;
  }
  parts = this.split(pattern);
  for (var i = 0, iMax = parts.length; i < iMax; i++) {
    var part = parts[i];
    parts[i] = part.slice(0, 1).toUpperCase() + part.slice(1).toLowerCase();
  }
  return parts.join('');
};

// Date

Date.prototype.yyyymmdd = function(sep) {
  var y = this.getYear() + 1900;
  var m = this.getMonth() + 1;
  var d = this.getDate();
  return '' + y + sep + (m < 10 ? '0' : '') + m + sep + (d < 10 ? '0' : '') + d;
};

// Return a human friendly string describing the given time difference t.
Date.prototype.friendlyAge = function(origin) {
  if (origin === null || origin === undefined) {
    origin = Date.now()
  }
  t = +origin - +this;
  var suffix = 'ago';
  if (t < 0) {
    suffix = 'in the future';
    t = -t;
  }
  if (t < 1) {
    return '0ms';
  }
  for (var pair of Date.prototype.friendlyAge_periods) {
    var T = pair[0], periodName = pair[1];
    if (t >= T) {
      var x = t / T;
      x = (x * 10 | 0) / 10;  // Reduce some decimal points.
      return '' + x + ' ' + periodName + (x > 1 ? 's ' : ' ') + suffix;
    }
  }
  return '?';  // should never happen.
};

Date.prototype.friendlyAge_periods = [
  [1000*60*60*24*365, 'year'],
  [1000*60*60*24*30, 'month'],
  [1000*60*60*24*7, 'week'],
  [1000*60*60*24, 'day'],
  [1000*60*60, 'hour'],
  [1000*60, 'minute'],
  [1000, 'second'],
  [1, 'millisecond'],
];

// NodeList:

NodeList.prototype.shallowCopy = function() {
  return Array.prototype.slice.call(this, 0);
};

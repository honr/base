// sat sun mon tue wed thu fri
// 0   1   2   3   4   5   6

// Busy engineer's calendar:
// '[week number since (epoch + 3 days)].[day of week] ' +
// '[high (5.625 minutes)][low (1.318 seconds)]'

// 2349.0-6 0-288.0-300
// 2349mo

//
// (/ 86400.0 (* 256 256))
// (/ (* 256 1.318359375) 60.0)
// 5.625

// (* 256 5.625 60)

// (* 24 12)

// (/ 60 5)

var SECONDS_PER_DAY = 24 * 3600;
var SMALLS_PER_DAY = 65536;

var trunctToDay = function(dateSec) {
  return Math.floor(dateSec / SECONDS_PER_DAY);
};

var startOfWeek = function(weekNum) {
  return new Date(1000.0 * SECONDS_PER_DAY * (2 + 7 * weekNum));
};

var weekOf = function(dayNum) {
  return Math.floor((dayNum - 2) / 7);
};

// var getWeekNumberStr = function(date) {
//   return 'w' + weekOf(trunctToDay(+date / 1000.0));
// };

// var dateFormatter = d3.time.format("%Y-%m-%d");

let engFromNormal = function(date) {
  var secs = Math.floor(+date / 1000.0 - 2 * 86400);
  var days = Math.floor(secs / 86400);
  var week = Math.floor(days / 7);
  var day = days - week * 7;
  var small = Math.floor((secs / SECONDS_PER_DAY - days) * SMALLS_PER_DAY);
  return 'w' + week + '.' + day + ' ' + small.toString(16);
};

let engToNormal = function(engDate) {
};

let normalDateInput = document.querySelector('input[name=normal]');
let engDateInput = document.querySelector('input[name=eng]');
let engClock = document.querySelector('.eng.clock');

normalDateInput.addEventListener(
  'change', function(e) {
    console.log('~ change', e, this);
    engDateInput.value = engFromNormal(new Date(this.value));
  });

engDateInput.addEventListener(
  'change', function(e) {
    console.log('~ change', e, this);
    normalDateInput.value = '';
  });

let engClockInterval = setInterval(function() {
  engClock.innerText = engFromNormal(new Date);
}, 1000);

#!/bin/sh

function set_emacs_theme() {
  local emacs_theme="$1"
  emacsclient --eval "(progn (setq custom-enabled-themes nil) (load-theme '$emacs_theme))" >/dev/null
}

function set_alacritty_theme() {
  local theme="$1"
  mkdir -p "$HOME/.config/alacritty"
  ( echo "general.import = ["
    echo "  \"$HOME/Pool/base/etc/alacritty-common.toml\","
    echo "  \"$HOME/Pool/base/etc/alacritty-${theme}.toml\","
    echo "]"
  ) > "$HOME/.config/alacritty/alacritty.toml"
}

function set_sway_theme() {
  if [[ -z "$WAYLAND_DISPLAY" ]] ; then
    return
  fi
  local theme="$1"
  gawk -e '
      BEGIN {s = "before"}
      s == "within" && /^\}/ {s = "after"}
      s == "within" && /^[ ]*[^ #].*/ {print $0}
      s == "before" && /^theme '$theme' \{/ {s = "within"}
    ' \
       <$HOME/Pool/base/i3/sway.themes \
       >$HOME/.config/sway/theme
  swaymsg reload
}

function set_x_theme() {
  if [[ "$UNAME" != "linux" ]] ; then
    return
  fi
  if [[ -n "$WAYLAND_DISPLAY" ]] ; then
    return
  fi
  local x_theme="$1"

  xrdb -load "$HOME/.Xresources"
  xrdb -merge "$HOME/Pool/base/etc/Xresources-${x_theme}"
  root_bg="$(xrdb -get root.bg)"
  if [[ -n "$root_bg" ]]; then
    xsetroot -solid "$root_bg"
  fi
  i3-msg -q restart
}

function set_gtk_theme() {
  if [[ "$UNAME" != "linux" ]] ; then
    return
  fi
  local gtk_theme="$1"
  gsettings set org.gnome.desktop.interface gtk-theme "$gtk_theme"
}

function set_macos_theme() {
  if [[ "$UNAME" != "darwin" ]] ; then
    return
  fi
  local macos_theme="$1"

  case "$macos_theme" in
    light)
      osascript -e "tell application \"System Events\" to tell appearance preferences to set dark mode to false"
      ;;
    dark)
      osascript -e "tell application \"System Events\" to tell appearance preferences to set dark mode to true"
      ;;
  esac
}

function main() {
  local theme_name="$1"

  case "$theme_name" in
       balcony)
         set_emacs_theme light-balcony
         set_gtk_theme Adwaita
         set_x_theme balcony
         set_sway_theme light
         set_macos_theme light
         ;;
       light)
         set_emacs_theme plain-light
         set_alacritty_theme light
         set_gtk_theme Adwaita
         set_x_theme light
         set_sway_theme light
         set_macos_theme light
         ;;
       dark)
         set_emacs_theme plain-dark
         set_alacritty_theme dark
         set_gtk_theme Adwaita-dark
         set_x_theme dark
         set_sway_theme dark
         set_macos_theme dark
         ;;
       *)
         echo -e "set-theme balcony|light|dark"
         return
         ;;
  esac
}

case "$1" in
  .*)
    verb="$1"
    shift 1
    "${verb#.}" "$@"
    ;;
  *) main "$@" ;;
esac

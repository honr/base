import os
import ptpython, prompt_toolkit
from prompt_toolkit.formatted_text import HTML

# import pygments.token, pygment.style
# from prompt_toolkit.filters import ViInsertMode
# from prompt_toolkit.key_binding.key_processor import KeyPress
# from prompt_toolkit.keys import Keys
# from prompt_toolkit.styles import Style
# from ptpython.layout import CompletionVisualisation

ZOO_DIR=os.path.expanduser('~/Zoo')

def configure(repl: ptpython.repl.PythonRepl):

  # Check pygments for list of colors, etc.
  # See: https://pygments.org/docs/styles/
  # Other places such as repl.use_code_colorscheme(.) also use pygments.
  class CustomPrompt(ptpython.prompt_style.PromptStyle):
    def in_prompt(self):
      i = repl.current_statement_index
      return HTML(f"<ansigreen>{i}.</ansigreen> ")

    def in2_prompt(self, width):
      gap = ''.rjust(width - 2)
      return HTML(f"<ansigreen>{gap}:</ansigreen> ")

    def out_prompt(self):
      return HTML(" ↳ ")


  repl.all_prompt_styles["custom"] = CustomPrompt()
  repl.prompt_style = "custom"
  repl.use_code_colorscheme("material") # or default for light background
  repl.color_depth = "DEPTH_8_BIT"  # 256 colors; DEPTH_24_BIT: True color.
  repl.min_brightness = 0.0  # Increase for dark terminal backgrounds.
  repl.max_brightness = 1.0  # Decrease for light terminal backgrounds.
  repl.enable_syntax_highlighting = True
  # Problem: these two are obstructive.  They should show in the toolbar.
  repl.show_signature = False
  repl.show_docstring = True
  # Show the "[Meta+Enter] Execute" message when pressing [Enter] only
  # inserts a newline instead of executing the code.
  repl.show_meta_enter_message = True
  # Show completions. (NONE, POP_UP, MULTI_COLUMN or TOOLBAR)
  toolbar = ptpython.layout.CompletionVisualisation.TOOLBAR
  repl.completion_visualisation = toolbar
  # When CompletionVisualisation.POP_UP has been chosen, use this
  # scroll_offset in the completion menu.
  repl.completion_menu_scroll_offset = 0
  repl.enable_system_bindings = True  # Enables Control-Z suspend.
  repl.show_line_numbers = False
  repl.show_status_bar = True
  repl.show_sidebar_help = True
  repl.swap_light_and_dark = False
  repl.highlight_matching_parenthesis = True
  repl.wrap_lines = True
  repl.enable_mouse_support = False
  repl.complete_while_typing = False
  repl.enable_fuzzy_completion = False
  repl.enable_dictionary_completion = False
  repl.vi_mode = False
  repl.paste_mode = False  # When True, don't insert whitespace after new line.
  repl.insert_blank_line_after_output = False
  repl.enable_open_in_editor = True
  repl.confirm_exit = False
  repl.enable_input_validation = True
  repl.vi_start_in_navigation_mode = False
  repl.vi_keep_last_used_mode = False
  repl.enable_history_search = True  # TODO: Change the bindings to M-p and M-n.
  # TODO: Find how to cycle through suggestions.
  repl.enable_auto_suggest = True

  # Custom key binding for some simple autocorrection while typing.
  """
  corrections = {
    "impotr": "import",
    "pritn": "print",
  }

  @repl.add_key_binding(" ")
  def _(event):
    " When a space is pressed. Check & correct word before cursor. "
    b = event.cli.current_buffer
    w = b.document.get_word_before_cursor()

    if w is not None:
      if w in corrections:
        b.delete_before_cursor(count=len(w))
        b.insert_text(corrections[w])

    b.insert_text(" ")
  """
  repl.title = '#' + os.getcwd().removeprefix(ZOO_DIR + '/') + ' '

ptpython.repl.embed(
  globals(),
  locals(),
  history_filename=os.path.join(os.getcwd(), 'history'),
  configure=configure)

# import rlcompleter
# import readline

# readline.parse_and_bind('tab: complete')

# import os, shutil, sys
# import re
# # import dill
# import gnureadline as readline
# # import rlcompleter, readline, atexit
# import rlcompleter, atexit
# import code # for InteractiveConsole

# # if 'STY' in os.environ:
# #   __sty = os.environ['STY']
# #   __sessions_dir = os.path.join(os.path.expanduser('~'),
# #                                 ".cache/python_sessions")
# #   __session_pickle_file = os.path.join(__sessions_dir, "{0}.pkl".format(__sty))
# #   print('# Loading session: {0}'.format(__sty))

# #   if os.path.exists(__session_pickle_file):
# #     dill.load_session(__session_pickle_file)

# #   def __save_session_to_pickle():
# #     os.makedirs(__sessions_dir, mode=0o700, exist_ok=True)
# #     dill.dump_session(__session_pickle_file)

# #   atexit.register(__save_session_to_pickle)

# historyPath = os.path.expanduser("~/.history/py-ml")
# # readline.parse_and_bind('tab: complete')
# readline.parse_and_bind('''
# tab: complete

# # set bell-style visible
# set bell-style none
# set match-hidden-files off
# set mark-symlinked-directories on
# set show-all-if-ambiguous on
# set blink-matching-paren on
# set completion-ignore-case On

# "\e[A": history-search-backward
# "\e[B": history-search-forward
# "\eOA": history-search-backward
# "\eOB": history-search-forward
# "\eOC": forward-char
# "\eOD": backward-char
# "\e[A": history-search-backward
# "\e[B": history-search-forward
# "\e[C": forward-char
# "\e[D": backward-char

# "\eN"  non-incremental-forward-search-history
# "\eP"  non-incremental-reverse-search-history
# "\en"  history-search-forward
# "\ep"  history-search-backward

# Control-w: kill-region
# ''')

# # if os.uname()[0] == 'Darwin':
# #     readline.parse_and_bind('bind ^I rl_complete')
# # else:
# #     readline.parse_and_bind('tab: complete')

# if os.path.exists(historyPath):
#     readline.read_history_file(historyPath)

# def save_history(historyPath=historyPath):
#     readline.write_history_file(historyPath)

# atexit.register(save_history)

# header = "Python "
# footer = "Bye."

# scope_vars = {
#   "answer": 42,
#   readline: readline,
# }

# code.InteractiveConsole(locals=scope_vars).interact(header, footer)

# class SomeStyle(pygments.style.Style):
#     styles = {
#         pygments.token.Token:                  '',
#         pygments.token.Comment:                'italic #888',
#         pygments.token.Keyword:                'bold #005',
#         pygments.token.Name:                   '#f00',
#         pygments.token.Name.Class:             'bold #0f0',
#         pygments.token.Name.Function:          '#0f0',
#         pygments.token.String:                 'bg:#eee #111'
#     }

# # Custom colorscheme for the UI. See `ptpython/layout.py` and
# # `ptpython/style.py` for all possible tokens.
# _custom_ui_colorscheme = {
#   "prompt": "#66aa33 bold",
#   # Make the status toolbar red.
#   "status-toolbar": "bg:#dddddd #000000",
# }

# Install custom colorscheme named 'my-colorscheme' and use it.
# repl.install_ui_colorscheme(
#   "my-colorscheme", prompt_toolkit.styles.Style.from_dict({
#     # Blue prompt.
#     "prompt": "#66aa33 bold",
#     # Make the status toolbar red.
#     "status-toolbar": "bg:#888888 #000000",
#   }))
# repl.use_ui_colorscheme("my-colorscheme")

# Add custom key binding for PDB.
# @repl.add_key_binding("c-b")
# def _(event):
#     " Pressing Control-B will insert \"pdb.set_trace()\" "
#     event.cli.current_buffer.insert_text("\nimport pdb; pdb.set_trace()\n")

# Typing ControlE twice should also execute the current command.
# (Alternative for Meta-Enter.)
"""
@repl.add_key_binding("c-e", "c-e")
def _(event):
    event.current_buffer.validate_and_handle()
"""

# Typing 'jj' in Vi Insert mode, should send escape. (Go back to navigation
# mode.)
"""
@repl.add_key_binding("j", "j", filter=ViInsertMode())
def _(event):
    " Map 'jj' to Escape. "
    event.cli.key_processor.feed(KeyPress(Keys("escape")))
"""

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"google.golang.org/protobuf/encoding/prototext"
	"google.golang.org/protobuf/proto"

	"honr.gitlab.com/base/sysc/systemd_unit"
)

func generateSystemdService(config *Unit) string {
	var sb strings.Builder

	sb.WriteString("[Unit]\n")
	sb.WriteString(fmt.Sprintf("Description=%s\n", config.Description))

	sb.WriteString("[Service]\n")
	sb.WriteString(fmt.Sprintf("ExecStart=%s\n", config.ExecStart))
	if config.User != "" {
		sb.WriteString(fmt.Sprintf("User=%s\n", config.User))
	}
	if config.Restart != "" {
		sb.WriteString(fmt.Sprintf("Restart=%s\n", config.Restart))
	}

	if len(config.Environment) > 0 {
		sb.WriteString("Environment=")
		for k, v := range config.Environment {
			sb.WriteString(fmt.Sprintf("\"%s=%s\" ", k, v))
		}
		sb.WriteString("\n") // Add a newline after the environment variables
	}


	sb.WriteString("[Install]\n")
	sb.WriteString("WantedBy=multi-user.target\n") // Or another appropriate target

	return sb.String()
}

func applyProto(config *Unit, serviceName string) {
	usr, err := user.Current()
	if err != nil {
		fmt.Println("Error getting user info:", err)
		return
	}

	systemdUserDir := filepath.Join(usr.HomeDir, ".config", "systemd", "user")
	serviceFile := filepath.Join(systemdUserDir, serviceName+".service")

	// Write the service file
	err = ioutil.WriteFile(serviceFile, []byte(generateSystemdService(config)), 0644)
	if err != nil {
		fmt.Println("Error writing service file:", err)
		return
	}

	// Handle wanted_by
	wantsDir := filepath.Join(systemdUserDir) // Check all wants directories.
	if config.Install != nil {
		for _, wantedBy := range config.Install.WantedBy {
			wantsFile := filepath.Join(wantsDir, wantedBy+".wants", serviceName+".service")
			err := os.Symlink(serviceFile, wantsFile)
			if err != nil && !os.IsExist(err) { // Ignore if symlink already exists
				fmt.Println("Error creating symlink:", err)
			}
		}
	} else {
		// Remove existing symlinks
		matches, _ := filepath.Glob(filepath.Join(wantsDir, "*.wants", serviceName+".service"))
		for _, match := range matches {
			err := os.Remove(match)
			if err != nil {
				fmt.Println("Error removing symlink:", err)
			}
		}
	}
}

func parseSystemdService(serviceContent string, config *systemd_unit.Unit) error {
	lines := strings.Split(serviceContent, "\n")
	section := ""
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if line == "" || strings.HasPrefix(line, "#") {
			continue // Skip empty lines and comments
		}

		if strings.HasPrefix(line, "[") && strings.HasSuffix(line, "]") {
			section = line[1 : len(line)-1]
			continue // Start of a new section
		}

		parts := strings.SplitN(line, "=", 2)
		if len(parts) != 2 {
			continue // Invalid line
		}

		key := strings.TrimSpace(parts[0])
		value := strings.TrimSpace(parts[1])

		switch section {
		case "Unit":
			parseUnitLine(config, key, value)
		case "Service":
			if config.Service == nil {
				config.Service = &systemd_unit.Service{}
			}
			parseServiceLine(config.Service, key, value)
		case "Install":
			if config.Install == nil {
				config.Install = &systemd_unit.Install{}
			}
			parseInstallLine(config.Install, key, value)

			// Add cases for other sections as needed.
		}
	}
	return nil
}

func parseUnitLine(config *systemd_unit.Unit, key, value string) {
	switch key {
	case "Description":
		config.Description = value
	case "Documentation":
		config.Documentation = append(config.Documentation, value)
	case "Requires":
		config.Requires = append(config.Requires, value)
	case "Wants":
		config.Wants = append(config.Wants, value)
	case "BindsTo":
		config.BindsTo = append(config.BindsTo, value)
	case "After":
		config.After = append(config.After, value)
	case "Before":
		config.Before = append(config.Before, value)
	case "RequiresMountsFor":
		config.RequiresMountsFor = append(config.RequiresMountsFor, value)
	case "OnFailure":
		config.OnFailure = value
	case "OnSuccess":
		config.OnSuccess = value
	case "JobTimeoutAction":
		config.JobTimeoutAction = value
	case "DefaultDependencies":
		config.DefaultDependencies = value
	case "Refs":
		config.Refs = value
	default:
		// Handle other Unit directives as needed, especially for the Properties map
		r := regexp.MustCompile(`^([a-zA-Z0-9-_]+)=(.*)$`)
		match := r.FindStringSubmatch(value)
		if match != nil {
			if config.Properties == nil {
				config.Properties = make(map[string]string)
			}
			config.Properties[match[1]] = match[2]
		}
	}
}

func parseServiceLine(service *systemd_unit.Service, key, value string) {
	switch key {
	case "Type":
		service.Type = value
	case "ExecStart":
		service.ExecStart = value
	case "ExecStartPre":
		service.ExecStartPre = append(service.ExecStartPre, value)
	case "ExecStartPost":
		service.ExecStartPost = append(service.ExecStartPost, value)
	case "ExecReload":
		service.ExecReload = value
	case "ExecStop":
		service.ExecStop = append(service.ExecStop, value)
	case "ExecStopPost":
		service.ExecStopPost = append(service.ExecStopPost, value)
	case "ExecCondition":
		service.ExecCondition = value
	case "Restart":
		service.Restart = value
	case "RestartSec":
		service.RestartSec = value
	case "TimeoutStartSec":
		service.TimeoutStartSec = value
	case "TimeoutStopSec":
		service.TimeoutStopSec = value
	case "User":
		service.User = value
	case "Group":
		service.Group = value
	case "PrivateTmp":
		service.PrivateTmp = value == "yes" || value == "true" || value == "1"
	case "Environment":
		parts := strings.SplitN(value, "=", 2)
		if len(parts) == 2 {
			if service.Environment == nil {
				service.Environment = make(map[string]string)
			}
			service.Environment[parts[0]] = parts[1]
		}
		// ... other Service fields
	}
}

func parseInstallLine(install *systemd_unit.Install, key, value string) {
	switch key {
	case "WantedBy":
		install.WantedBy = append(install.WantedBy, value)
	case "RequiredBy":
		install.RequiredBy = append(install.RequiredBy, value)
	case "Alias":
		install.Alias = value
	case "Also":
		install.Also = value
		// ... other Install fields
	}
}

func main() {
	fromProto := flag.String("from-proto", "", "Input .textproto file")
	parseService := flag.String("parse-service", "", "Path to .service file")
	apply := flag.Bool("apply", false, "Apply changes to user systemd directory")
	flag.Parse()

	if *fromProto == "" && *parseService == "" {
		fmt.Println("Usage: go run main.go (--from-proto INPUT_TEXTPROTO_FILE | --parse-service PATH_TO_SERVICE_FILE) [--apply]")
		return
	}

	if *fromProto != "" {
		config := &Unit{}
		in, err := ioutil.ReadFile(*fromProto)
		if err != nil {
			fmt.Println("Error reading input file:", err)
			return
		}
		if err := prototext.Unmarshal(in, config); err != nil {
			fmt.Println("Error unmarshaling protobuf:", err)
			return
		}

		serviceContent := generateSystemdService(config)
		fmt.Print(serviceContent)

		if *apply {
			applyProto(config, filepath.Base(*fromProto[:len(*fromProto)-len(filepath.Ext(*fromProto))]))
		}

	} else if *parseService != "" {
		config := &systemd_unit.Unit{}
		in, err := ioutil.ReadFile(*parseService)
		if err != nil {
			fmt.Println("Error reading service file:", err)
			return
		}

		err = parseSystemdService(string(in), config)
		if err != nil {
			fmt.Println("Error parsing service file:", err)
			return
		}

		out, err := prototext.MarshalOptions{Multiline: true}.Marshal(config)
		if err != nil {
			fmt.Println("Error marshaling to textproto:", err)
			return
		}
		fmt.Print(string(out))
	}
}

#include <string.h>
#include <Evas.h>
#include <Ecore.h>
#include <Ecore_Evas.h>
#include <freetype2/ft2build.h>
#include <harfbuzz/hb.h>
#include <harfbuzz/hb-ft.h>
#include <stdio.h> // For error printing

int main(int argc, char **argv) {
  // EFL Initialization
  if (!ecore_init()) {
    fprintf(stderr, "Ecore initialization failed!\n");
    return 1;
  }
  if (!ecore_evas_init()) {
    fprintf(stderr, "ERROR: Could not initialize Ecore_Evas.\n");
    ecore_shutdown();
    return 1;
  }
  // Get the list of available Ecore_Evas engines.
  Eina_List* available_engines = ecore_evas_engines_get();
  if (available_engines != NULL) {
    for (const Eina_List* l = available_engines;
         l != NULL;
         l = eina_list_next(l)) {
      char* s = (char*) l->data;
      printf("  engine: %s\n", s);
    }
    ecore_evas_engines_free(available_engines);
  }

  /* Evas_Engine_Info_Software_X11 *einfo; */
  /* Evas *evas = evas_new(); */
  /* Evas* evas = evas_open(ecore_evas_new(100, 100)); */
  /* if (!evas) { */
  /*   fprintf(stderr, "Evas creation failed!\n"); */
  /*   ecore_shutdown(); */
  /*   return 1; */
  /* } */
  /* printf("%s", evas_engine_info_get(evas)->name); */

  /* // Get engine info (important for setting up the canvas) */
  /* einfo = (Evas_Engine_Info_Software_X11 *)evas_engine_info_get(evas); */
  /* if (!einfo) { */
  /*   fprintf(stderr, "Failed to get Evas engine info!\n"); */
  /*   evas_free(evas); */
  /*   ecore_shutdown(); */
  /*   return 1; */
  /* } */

  // Create a window
  Ecore_Evas *ee = ecore_evas_new("software_x11", 0, 0, 800, 600, NULL); // Adjust size as needed
  if (ee==NULL) {
    fprintf(stderr, "Ecore_Evas creation failed!\n");
    // evas_free(evas);
    ecore_shutdown();
    return 1;
  }
  ecore_evas_title_set(ee, "HarfBuzz with EFL");
  ecore_evas_show(ee);
  evas_object_show(ecore_evas_get(ee)); // Show the underlying Evas canvas

  /* // Set the evas on the engine info. */
  /* einfo->info.screen.w = 800; // Set width and height */
  /* einfo->info.screen.h = 600; */
  /* evas_engine_info_set(evas, einfo); */


  /* // FreeType initialization */
  /* FT_Library ft; */
  /* if (FT_Init_FreeType(&ft)) { */
  /*   fprintf(stderr, "FreeType initialization failed!\n"); */
  /*   evas_free(evas); */
  /*   ecore_shutdown(); */
  /*   return 1; */
  /* } */

  /* FT_Face ft_face; */
  /* if (FT_New_Face(ft, "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", 0, &ft_face)) { // Replace with your font path */
  /*   fprintf(stderr, "Font loading failed!\n"); */
  /*   FT_Done_FreeType(ft); */
  /*   evas_free(evas); */
  /*   ecore_shutdown(); */
  /*   return 1; */
  /* } */

  /* // HarfBuzz setup */
  /* hb_font_t *hb_font = hb_ft_font_create(ft_face, NULL); */
  /* hb_buffer_t *hb_buffer = hb_buffer_create(); */

  /* const char *text = "مرحبا بالعالم"; // Example Arabic text */

  /* hb_buffer_add_utf8(hb_buffer, text, -1, 0, -1); */
  /* hb_shape(hb_font, hb_buffer, NULL, 0); */

  /* unsigned int glyph_count = hb_buffer_get_length(hb_buffer); */
  /* hb_glyph_info_t *glyph_info = hb_buffer_get_glyph_infos(hb_buffer, NULL); */
  /* hb_glyph_position_t *glyph_pos = hb_buffer_get_glyph_positions(hb_buffer, NULL); */

  /* int x = 100, y = 100; */

  /* for (unsigned int i = 0; i < glyph_count; ++i) { */
  /*   FT_UInt glyph_index = glyph_info[i].codepoint; */

  /*   if (FT_Load_Glyph(ft_face, glyph_index, FT_LOAD_DEFAULT)) { */
  /*     fprintf(stderr, "Glyph loading failed!\n"); */
  /*     continue; // Skip to the next glyph */
  /*   } */

  /*   if (FT_Render_Glyph(ft_face->glyph, FT_RENDER_MODE_NORMAL)) { */
  /*     fprintf(stderr, "Glyph rendering failed!\n"); */
  /*     continue; */
  /*   } */

  /*   FT_Bitmap *bitmap = &ft_face->glyph->bitmap; */

  /*   Evas_Object *text_object = evas_object_image_add(evas); */
  /*   evas_object_image_size_set(text_object, bitmap->width, bitmap->rows); */

  /*   if (bitmap->buffer) { // Check if bitmap data exists */
  /*     evas_object_image_data_set(text_object, bitmap->buffer); */
  /*   } else { */
  /*     fprintf(stderr, "Bitmap buffer is null!\n"); */
  /*   } */


  /*   evas_object_move(text_object, x + glyph_pos[i].x_offset / 64.0, y + glyph_pos[i].y_offset / 64.0); */
  /*   evas_object_show(text_object); */

  /*   x += glyph_pos[i].x_advance / 64.0; */
  /* } */

  /* hb_buffer_destroy(hb_buffer); */
  /* hb_font_destroy(hb_font); */
  /* FT_Done_Face(ft_face); */
  /* FT_Done_FreeType(ft); */

  /* // Main loop */
  /* ecore_main_loop_begin(); */

  /* // Shutdown */
  /* ecore_main_loop_end(); */
  /* evas_free(evas); */
  /* ecore_shutdown(); */

  return 0;
}

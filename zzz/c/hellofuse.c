// cc -o hellofuse hellofuse.c -Wall -Wextra -D_FILE_OFFSET_BITS=64 `pkg-config --cflags --libs fuse3`

#define FUSE_USE_VERSION 31

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

static const char *filepath = "/hello";
static const char *filecontent = "Hello, FUSE!";

static int my_getattr(const char *path, struct stat *stbuf,
                     struct fuse_file_info *fi) {
  (void) fi;
  if (strcmp(path, "/") == 0) {
    stbuf->st_mode = S_IFDIR | 0755;
    stbuf->st_nlink = 2;
  } else if (strcmp(path, filepath) == 0) {
    stbuf->st_mode = S_IFREG | 0444;
    stbuf->st_nlink = 1;
    stbuf->st_size = strlen(filecontent);
  } else {
    return -ENOENT;
  }
  return 0;
}

static int my_open(const char *path, struct fuse_file_info *fi) {
  if (strcmp(path, filepath) != 0) {
    return -ENOENT;
  }

  if ((fi->flags & O_ACCMODE) != O_RDONLY) {
        return -EACCES; // Only read access allowed
  }
  return 0;
}

static int my_read(const char *path, char *buf, size_t size, off_t offset,
                    struct fuse_file_info *fi) {
  (void) fi;
  if (strcmp(path, filepath) != 0) {
    return -ENOENT;
  }
  if (offset < 0) {
    return 0;
  }
  off_t len = (off_t) strlen(filecontent);
  if (offset >= len) {
    return 0;
  }
  if (offset + (off_t) size > len) {
    size = len - offset;
  }
  memcpy(buf, filecontent + offset, size);
  return size;
}

static struct fuse_operations my_oper = {
  .getattr = my_getattr,
  .open = my_open,
  .read = my_read,
};

int main(int argc, char *argv[]) {
  return fuse_main(argc, argv, &my_oper, NULL);
}

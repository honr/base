-std=c++23
-fno-rtti # Disables runtime type information (RTTI).
-fno-exceptions # Disables exception handling.
-fno-threadsafe-statics # Disables thread-safe initialization of local static variables. (Might not be relevant if you're aiming for pure C behavior.)
-Wno-c++-compat: Suppresses warnings about constructs that have different behavior in C and C++. (Use with caution, as it might hide legitimate issues.)
-fno-strict-aliasing: Relaxes strict aliasing rules, which can be stricter in C++. (Again, use with caution.)
Enforcing C-like behavior:

-Wwrite-strings: Warns about attempts to modify string literals (which are allowed in C++ but not in C).
-Wimplicit-fallthrough: Warns about fall-through cases in switch statements (which are allowed in C but considered bad practice in many cases).
-Wold-style-cast: Warns about C-style casts.
-Wc++-compat: Warns about constructs that have different behavior in C and C++. (This is the opposite of -Wno-c++-compat and can be useful for identifying potential issues.)
Linking:

Link with the C standard library (libc) instead of the C++ standard library (libstdc++). This might require compiler-specific options.

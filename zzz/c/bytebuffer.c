#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define INITIAL_CAPACITY 128

typedef struct {
  char* data;
  size_t size;      // Current size of data
  size_t capacity;  // Total allocated capacity
} ByteBuffer;

ByteBuffer* byte_buffer_new(size_t initial_capacity) {
  ByteBuffer* bb = malloc(sizeof(ByteBuffer));
  if (!bb) {
    perror("malloc failed");
    return NULL;
  }

  bb->data = malloc(initial_capacity);
  if (!bb->data) {
    perror("malloc failed");
    free(bb);
    return NULL;
  }

  bb->size = 0;
  bb->capacity = initial_capacity;
  return bb;
}

void byte_buffer_free(ByteBuffer* bb) {
  free(bb->data);
  free(bb);
}

int byte_buffer_read_from_fd(ByteBuffer* bb, int fd) {
  while (1) {
    if (bb->size == bb->capacity) {
      size_t new_capacity = bb->capacity * 2;
      char* new_data = realloc(bb->data, new_capacity);
      if (!new_data) {
        perror("realloc failed");
        return -1;
      }
      bb->data = new_data;
      bb->capacity = new_capacity;
    }

    ssize_t nread = read(fd, bb->data + bb->size, bb->capacity - bb->size);
    if (nread == -1) {
      if (errno == EINTR) continue;  // Interrupted, try again
      perror("read failed");
      return -1;
    }
    if (nread == 0) {
      return 0;  // EOF
    }
    bb->size += nread;
  }
}

int main() {
  ByteBuffer* bb = byte_buffer_new(INITIAL_CAPACITY);
  if (!bb) return 1;

  int fd = 0;  // stdin
  if (byte_buffer_read_from_fd(bb, fd) == -1) {
    byte_buffer_free(bb);
    return 1;
  }

  // Example: print the contents (for testing)
  // write(1, bb->data, bb->size); // write to stdout

  byte_buffer_free(bb);
  return 0;
}

// Unit tests (using assert for simplicity)
#ifdef UNIT_TESTS

#include <assert.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

void test_byte_buffer() {
  ByteBuffer* bb = byte_buffer_new(INITIAL_CAPACITY);
  assert(bb != NULL);
  assert(bb->size == 0);
  assert(bb->capacity == INITIAL_CAPACITY);

  int fd =
      open("test_file.txt", O_RDONLY | O_CREAT, 0644);  // Create a test file
  assert(fd != -1);
  const char* test_data = "This is a test string.";
  write(fd, test_data, strlen(test_data));
  close(fd);

  fd = open("test_file.txt", O_RDONLY);
  assert(fd != -1);
  int ret = byte_buffer_read_from_fd(bb, fd);
  assert(ret == 0);
  close(fd);

  assert(bb->size == strlen(test_data));
  assert(memcmp(bb->data, test_data, bb->size) == 0);

  byte_buffer_free(bb);
  remove("test_file.txt");  // Clean up
}

int main() {
  test_byte_buffer();
  return 0;
}
#endif

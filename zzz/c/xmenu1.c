#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define MAX_ITEMS 256
#define MAX_ITEM_LEN 256

int main(int argc, char *argv[]) {
  Display *display;
  Window root, win;
  XEvent event;
  int screen;
  GC gc;
  XFontStruct *font_info;
  char items[MAX_ITEMS][MAX_ITEM_LEN];
  int num_items = 0;
  int selected_item = -1;
  int i;

  if (argc > 1) {
    for (i = 1; i < argc && num_items < MAX_ITEMS; i++) {
      strncpy(items[num_items], argv[i], MAX_ITEM_LEN - 1);
      items[num_items][MAX_ITEM_LEN - 1] = '\0'; // Ensure null termination
      num_items++;
    }
  } else {
    // Read items from stdin (one per line)
    while (fgets(items[num_items], MAX_ITEM_LEN, stdin) != NULL && num_items < MAX_ITEMS) {
      items[num_items][strcspn(items[num_items], "\n")] = 0; // Remove newline
      num_items++;
    }
  }

  if (num_items == 0) {
    return 1; // No items to display
  }


  display = XOpenDisplay(NULL);
  if (display == NULL) {
    fprintf(stderr, "Cannot open display\n");
    return 1;
  }

  screen = DefaultScreen(display);
  root = RootWindow(display, screen);

  font_info = XLoadQueryFont(display, ""); // Or any other font
  if (!font_info) {
    fprintf(stderr, "Cannot load font\n");
    XCloseDisplay(display);
    return 1;
  }


  win = XCreateSimpleWindow(display, root, 10, 10, 200, num_items * (font_info->ascent + font_info->descent) + 10, 1,
                            BlackPixel(display, screen), WhitePixel(display, screen));

  XSelectInput(display, win, ExposureMask | KeyPressMask);
  XMapWindow(display, win);

  gc = XCreateGC(display, win, 0, NULL);
  XSetFont(display, gc, font_info->fid);

  while (1) {
    XNextEvent(display, &event);
    switch (event.type) {
      case Expose:
        for (i = 0; i < num_items; i++) {
          XDrawString(display, win, gc, 5, 5 + i * (font_info->ascent + font_info->descent) + font_info->ascent, items[i], strlen(items[i]));
          if (i == selected_item) {
            XDrawRectangle(display, win, gc, 2, 2 + i * (font_info->ascent + font_info->descent), 196, font_info->ascent + font_info->descent); // Highlight selection
          }
        }
        break;
      case KeyPress:
        {
          KeySym keysym = XKeycodeToKeysym(display, event.xkey.keycode, 0);
          if (keysym == XK_Up) {
            selected_item = (selected_item - 1 + num_items) % num_items; // Wrap around
            XClearWindow(display, win); // Redraw with new selection
          } else if (keysym == XK_Down) {
            selected_item = (selected_item + 1) % num_items; // Wrap around
            XClearWindow(display, win); // Redraw with new selection
          } else if (keysym == XK_Return) {
            if (selected_item != -1) {
              printf("%s\n", items[selected_item]);
            }
            XFreeGC(display, gc);
            XUnloadFont(display, font_info->fid);
            XDestroyWindow(display, win);
            XCloseDisplay(display);
            return 0;
          } else if (keysym == XK_Escape) {
            XFreeGC(display, gc);
            XUnloadFont(display, font_info->fid);
            XDestroyWindow(display, win);
            XCloseDisplay(display);
            return 1;
          }
        }
        break;
    }
  }

  return 0;
}

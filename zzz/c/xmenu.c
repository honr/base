#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* #include <X11/Xft/Xft.h> */
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <harfbuzz/hb.h>
#include <harfbuzz/hb-ft.h>
#include <fontconfig/fontconfig.h>
#include <ft2build.h>
#include FT_FREETYPE_H

#define MAX_ITEMS 256
#define MAX_ITEM_LEN 256


FT_Face ObtainFace(FT_Library ft, const char* face_name) {
  FT_Face face = NULL;
  FcConfig* config = FcConfigGetCurrent();
  if (config == NULL) {
    return NULL; // Handle error: Could not get font config
  }
  FcPattern* pattern = FcPatternCreate();
  if (pattern == NULL) {
    return NULL; // Handle error: Could not create pattern
  }
  FcPatternAddString(pattern, FC_FAMILY, (const FcChar8*)face_name);
  FcDefaultSubstitute(pattern);
  FcResult result;
  FcPattern* matches = FcFontMatch(config, pattern, &result);
  fprintf(stderr, "FcFontMatch result: %d\n", result);
  return NULL;

  /* FcObjectSet* os = FcObjectSetCreate(); */
  /* if (!os) { */
  /*   FcPatternDestroy(pattern); */
  /*   return NULL; // Handle error */
  /* } */
  /* FcObjectSetAdd(os, FC_FILE); // We want the filename */


  /* FcFontSet* fs = FcFontSetMatch(config, NULL, 0, pattern, &result); // Correct usage */
  /* if (!fs || result != FcResultMatch) {  // Check result! */
  /*   FcPatternDestroy(pattern); */
  /*   FcObjectSetDestroy(os); */
  /*   if (fs) { */
  /*     FcFontSetDestroy(fs); // Destroy if it was created */
  /*   } */
  /*   fprintf(stderr, "Error: No matching fonts found (or other Fontconfig error): FcResult=%d\n", result); */
  /*   return NULL; */
  /* } */

  /* FcFontSet* fs = FcFontSetMatch(config, pattern, os);  // Find matching fonts */
  /* if (!fs) { */
  /*   FcPatternDestroy(pattern); */
  /*   FcObjectSetDestroy(os); */
  /*   return NULL; // Handle error: No matching fonts found */
  /* } */

  /* FT_Face* face = NULL; */
  /* if (fs->nFont > 0) { */
  /*   FcChar8* filename = NULL; */
  /*   FcPattern *font_pattern = FcFontRenderPrepare(config, pattern, fs->fonts[0]); */
  /*   if (font_pattern) { */
  /*     if (FcPatternGetString(font_pattern, FC_FILE, 0, &filename) == FcResultMatch && filename) { */
  /*       if (FT_New_Face(ft, (const char*)filename, 0, &face) != 0) { */
  /*         // Handle FreeType error (e.g., invalid font file) */
  /*         face = NULL; // Important: Set face to null on error */
  /*       } */
  /*     } */
  /*     FcPatternDestroy(font_pattern); */
  /*   } */
  /* } */

  /* FcFontSetDestroy(fs); */
  /* FcPatternDestroy(pattern); */
  /* FcObjectSetDestroy(os); */

  return face;
}


/* FT_Face* ObtainFace(FT_Library ft, const char* face_name) { */
/*   FcConfig *fc_config = FcConfigCreate(); // Use FcConfigCreate() */
/*   FcPattern *fc_pattern = FcPatternCreate(); */
/*   FcPatternAddString(fc_pattern, FC_FAMILY, (const FcChar8*)face_name); */

/*   // Add other font properties as needed (FC_STYLE, FC_WEIGHT, FC_SIZE, etc.) */
/*   // Example: */
/*   // FcPatternAddString(fc_pattern, FC_STYLE, (const FcChar8*)"Bold"); */
/*   // FcPatternAddDouble(fc_pattern, FC_SIZE, 12.0); */

/*   FcObjectSet *fc_os = FcObjectSetCreate(); */
/*   FcObjectSetAdd(fc_os, FC_FILE);  // Essential: We need the filename */
/*   // Add other properties if needed (FC_FAMILY, FC_STYLE, etc.) */

/*   FcFontSet *fc_fonts = NULL; // Initialize to NULL */
/*   int font_count; */

/*   // Get the list of fonts */
/*   // fc_fonts = FcConfigGetFonts(fc_config, fc_os, &font_count); */
/*   fc_fonts = FcConfigGetFonts(fc_config, FcSetSystem); */

/*   FT_Face *face = NULL; */
/*   FcChar8 *filename = NULL; */

/*   if (fc_fonts && font_count > 0) { */
/*     FcFont *font = FcFontSetGet(fc_fonts, 0); // Get the first matching font */
/*     if (FcPatternGetString(font, FC_FILE, 0, &filename)) { */
/*       if (FT_New_Face(ft, (const char*)filename, 0, &face)) { */
/*         fprintf(stderr, "Could not open font: %s\n", (char*)filename); */
/*       } */
/*     } else { */
/*       fprintf(stderr, "Could not get font filename\n"); */
/*     } */
/*   } else { */
/*     fprintf(stderr, "No matching fonts found\n"); */
/*   } */

/*   if (fc_fonts) { // Check if fc_fonts is not NULL before destroying */
/*     FcFontSetDestroy(fc_fonts); */
/*   } */
/*   FcPatternDestroy(fc_pattern); */
/*   FcConfigDestroy(fc_config); */
/*   FcObjectSetDestroy(fc_os); */

/*   return face; */
/* } */

/* FT_Face* ObtainFace(FT_Library ft, const char* face_name) { */
/*   FcConfig *fc_config = FcInitLoadDefault(); */
/*   FcPattern *fc_pattern = FcPatternCreate(); */

/*   // Example: Set font family (customize as needed) */
/*   FcPatternAddString(fc_pattern, FC_FAMILY, (const FcChar*)face_name); */

/*   // Add other font properties (style, weight, size, etc.) as needed */
/*   // Example for bold: */
/*   // FcPatternAddInteger(fc_pattern, FC_WEIGHT, FC_WEIGHT_BOLD); */
/*   // Example for size 12: */
/*   // FcPatternAddDouble(fc_pattern, FC_SIZE, 12.0); */


/*   FcObjectSet *fc_os = FcObjectSetCreate(); */
/*   FcObjectSetAdd(fc_os, FC_FILE); // Essential: We need the filename */
/*   // Add other properties if needed (FC_FAMILY, FC_STYLE, etc.) */

/*   FcFontSet *fc_fonts = FcFontListCreate(); */

/*   FcConfigSubstitute(fc_config, fc_pattern, FcMatchFont, fc_fonts); */
/*   FcDefaultSubstitute(fc_pattern); */
/*   FcFontSetSort(fc_fonts); */

/*   FT_Face *face = NULL; */
/*   FcChar8 *filename = NULL; */

/*   if (fc_fonts && FcFontSetGetCount(fc_fonts) > 0) { */
/*     FcFont *font = FcFontSetGet(fc_fonts, 0); */
/*     if (FcPatternGetString(font, FC_FILE, 0, &filename)) { */
/*       if (FT_New_Face(ft, (const char*)filename, 0, &face)) { */
/*         fprintf(stderr, "Could not open font: %s\n", (char*)filename); */
/*       } */
/*     } else { */
/*       fprintf(stderr, "Could not get font filename\n"); */
/*     } */
/*   } else { */
/*     fprintf(stderr, "No matching fonts found\n"); */
/*   } */

/*   FcFontSetDestroy(fc_fonts); */
/*   FcPatternDestroy(fc_pattern); */
/*   FcConfigDestroy(fc_config); */
/*   FcObjectSetDestroy(fc_os); */

/*   return face; // Return the FT_Face (or NULL on failure) */
/* } */

/* FT_Face* GetFace() { */
/*   // Fontconfig setup */
/*   FcConfig *fc_config = FcInitLoadConfig(); */
/*   FcPattern *fc_pattern = FcNameParse((const FcChar8*)"Sans");  // Example font, customize as needed */
/*   FcObjectSet *fc_objectset = FcObjectSetCreate(); */
/*   FcObjectSetAdd(fc_os, FC_FILE); // Add FC_FILE to get the filename */
/*   FcObjectSetAdd(fc_os, FC_FAMILY); // Example: Add FC_FAMILY */
/*   FcObjectSetAdd(fc_os, FC_STYLE); // Example: Add FC_STYLE */
/*   FcObjectSetAdd(fc_os, FC_WEIGHT); // Example: Add FC_WEIGHT */
/*   FcObjectSetAdd(fc_os, FC_SIZE);  // Example: Add FC_SIZE */
/*   FcFontSet *fc_fonts = FcFontList(fc_config, fc_pattern, fc_objectset); */
/*   /\* FcConfigSubstituteWithPat(fc_config, fc_pattern, FcMatchFont, fc_fonts); *\/ */

/*   FcDefaultSubstitute(fc_pattern); */
/*   /\* FcFontSetSort(fc_fonts); *\/ */
/*   /\* FcFontSetSort(fc_config, &fc_fonts, 1, fc_pattern, False, ); *\/ */

/*   FcChar8 *filename = NULL; */
/*   int font_id = 0; */
/*   if (FcFontSetGetCount(fc_fonts) > 0) { */
/*     FcFont *font = FcFontSetGet(fc_fonts, 0); */
/*     FcResult result; */
/*     FcPatternGetFilename(font, 0, &filename); */
/*     if (FT_New_Face(ft, (const char*)filename, 0, &face)) { */
/*       fprintf(stderr, "Could not open font\n"); */
/*       XCloseDisplay(display); */
/*       FT_Done_FreeType(ft); */
/*       FcFontSetDestroy(fc_fonts); */
/*       FcPatternDestroy(fc_pattern); */
/*       FcConfigDestroy(fc_config); */
/*       return 1; */
/*     } */
/*   } else { */
/*     fprintf(stderr, "No fonts found\n"); */
/*     XCloseDisplay(display); */
/*     FT_Done_FreeType(ft); */
/*     FcFontSetDestroy(fc_fonts); */
/*     FcPatternDestroy(fc_pattern); */
/*     FcConfigDestroy(fc_config); */
/*     return 1; */
/*   } */


/*   FcFontSetDestroy(fc_fonts); */
/*   FcPatternDestroy(fc_pattern); */
/*   FcConfigDestroy(fc_config); */
/* } */

/* FT_Face* ObtainFace(FT_Library ft) { */
/*   FcConfig *fc_config = FcInitLoadDefault(); */
/*   FcPattern *fc_pattern = FcPatternCreate(); */
/*   FcPatternAddString(fc_pattern, FC_FAMILY, (const FcChar*)"Sans"); // Customize as needed */
/*   // Add other font properties (style, weight, size, etc.) */

/*   FcObjectSet *fc_os = FcObjectSetCreate(); */
/*   FcObjectSetAdd(fc_os, FC_FILE); // Essential: We need the filename */
/*   // Add other properties if needed (FC_FAMILY, FC_STYLE, etc.) */

/*   FcFontSet *fc_fonts = FcFontListCreate(); */
/*   FcConfigSubstitute(fc_config, fc_pattern, FcMatchFont, fc_fonts); */
/*   FcDefaultSubstitute(fc_pattern); */
/*   FcFontSetSort(fc_fonts); */

/*   FT_Face *face = NULL; */
/*   FcChar8 *filename = NULL; */

/*   if (fc_fonts && FcFontSetGetCount(fc_fonts) > 0) { */
/*     FcFont *font = FcFontSetGet(fc_fonts, 0); */
/*     if (FcPatternGetString(font, FC_FILE, 0, &filename)) { */
/*       if (FT_New_Face(ft, (const char*)filename, 0, &face)) { */
/*         fprintf(stderr, "Could not open font: %s\n", (char*)filename); */
/*       } */
/*     } else { */
/*       fprintf(stderr, "Could not get font filename\n"); */
/*     } */
/*   } else { */
/*     fprintf(stderr, "No matching fonts found\n"); */
/*   } */

/*   FcFontSetDestroy(fc_fonts); */
/*   FcPatternDestroy(fc_pattern); */
/*   FcConfigDestroy(fc_config); */
/*   FcObjectSetDestroy(fc_os); */

/*   return face; // Return the FT_Face (or NULL on failure) */
/* } */

int main(int argc, char *argv[]) {
  Display *display;
  Window root, win;
  XEvent event;
  int screen;
  GC gc;
  hb_font_t *hb_font;
  hb_buffer_t *hb_buf;

  char items[MAX_ITEMS][MAX_ITEM_LEN];
  int num_items = 0;
  int selected_item = -1;
  int i;

  if (argc > 1) {
    for (i = 1; i < argc && num_items < MAX_ITEMS; i++) {
      strncpy(items[num_items], argv[i], MAX_ITEM_LEN - 1);
      items[num_items][MAX_ITEM_LEN - 1] = '\0'; // Ensure null termination
      num_items++;
    }
  } else {
    // Read items from stdin (one per line)
    while (fgets(items[num_items], MAX_ITEM_LEN, stdin) != NULL && num_items < MAX_ITEMS) {
      items[num_items][strcspn(items[num_items], "\n")] = 0; // Remove newline
      num_items++;
    }
  }

  if (num_items == 0) {
    return 1; // No items to display
  }

  display = XOpenDisplay(NULL);
  if (display == NULL) {
    fprintf(stderr, "Cannot open display\n");
    return 1;
  }
  screen = DefaultScreen(display);
  root = RootWindow(display, screen);
  FT_Library ft;
  if (FT_Init_FreeType(&ft)) {
    fprintf(stderr, "Could not init FreeType\n");
    return 1;
  }
  FT_Face face = ObtainFace(ft, "Sans");
  if (face == NULL) {
    fprintf(stderr, "Failed to obtain a font face.\n");
    FT_Done_FreeType(ft);
    XCloseDisplay(display);
    return 1;
  }
  hb_font = hb_ft_font_create(face, NULL);

  win = XCreateSimpleWindow(display, root, 10, 10, 200, num_items * (face->size->metrics.height >> 6) + 10, 1,
                            BlackPixel(display, screen), WhitePixel(display, screen));

  XSelectInput(display, win, ExposureMask | KeyPressMask);
  XMapWindow(display, win);

  gc = XCreateGC(display, win, 0, NULL);


  while (1) {
    XNextEvent(display, &event);
    switch (event.type) {
      case Expose:
        for (i = 0; i < num_items; i++) {
          hb_buf = hb_buffer_create();
          hb_buffer_set_direction(hb_buf, HB_DIRECTION_LTR);
          hb_buffer_set_script(hb_buf, HB_SCRIPT_LATIN); // Adjust as needed
          hb_buffer_set_language(hb_buf, hb_language_from_string("en", -1)); // Adjust as needed

          hb_buffer_add_utf8(hb_buf, items[i], -1, 0, -1);
          hb_shape(hb_font, hb_buf, NULL, 0);

          unsigned int glyph_count;
          hb_glyph_info_t *glyph_info = hb_buffer_get_glyph_infos(hb_buf, &glyph_count);
          hb_glyph_position_t *glyph_pos = hb_buffer_get_glyph_positions(hb_buf, &glyph_count);

          int x = 5;
          for (unsigned int j = 0; j < glyph_count; j++) {
            XftDrawStringUtf8(draw, &color, xft_font, x, y + xft_font->ascent,
                              (const FcChar8*)text, strlen(text)); // Use xft_font
            XftDrawStringUtf8(display, win, DefaultVisual(display, screen), gc, x, 5 + i * (face->size->metrics.height >> 6) + (face->size->metrics.ascender >> 6), items[i], strlen(items[i]));
            x += glyph_pos[j].x_advance >> 6;
          }

          hb_buffer_destroy(hb_buf);
          if (i == selected_item) {
            XDrawRectangle(display, win, gc, 2, 2 + i * (face->size->metrics.height >> 6), 196, face->size->metrics.height >> 6); // Highlight selection
          }
        }
        break;
      case KeyPress:
        {
          KeySym keysym = XKeycodeToKeysym(display, event.xkey.keycode, 0);
          if (keysym == XK_Up) {
            selected_item = (selected_item - 1 + num_items) % num_items; // Wrap around
            XClearWindow(display, win); // Redraw with new selection
          } else if (keysym == XK_Down) {
            selected_item = (selected_item + 1) % num_items; // Wrap around
            XClearWindow(display, win); // Redraw with new selection
          } else if (keysym == XK_Return) {
            if (selected_item != -1) {
              printf("%s\n", items[selected_item]);
            }
            XFreeGC(display, gc);
            hb_font_destroy(hb_font);
            FT_Done_Face(face);
            FT_Done_FreeType(ft);
            XDestroyWindow(display, win);
            XCloseDisplay(display);
            return 0;
          } else if (keysym == XK_Escape) {
            XFreeGC(display, gc);
            hb_font_destroy(hb_font);
            FT_Done_Face(face);
            FT_Done_FreeType(ft);
            XDestroyWindow(display, win);
            XCloseDisplay(display);
            return 1;
          }
        }
        break;
    }
  }

  return 0;
}

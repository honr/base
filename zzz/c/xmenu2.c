#include <Evas.h>
#include <Ecore.h>
#include <fontconfig/fontconfig.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include <harfbuzz/hb-ft.h>
#include <harfbuzz/hb.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// It is more than face_name at the moment.  (FcNameParse).
FT_Face ObtainFace(FT_Library ft, const char* face_name) {
  FT_Face ft_face = NULL;
  // FcConfig* config = FcConfigGetCurrent();
  FcConfig* config = FcInitLoadConfigAndFonts();
  if (config == NULL) {
    return NULL; // Handle error: Could not get font config
  }
  FcPattern* pattern = FcNameParse((FcChar8*)face_name);
  /* FcPattern* pattern = FcPatternCreate(); */
  if (pattern == NULL) {
    return NULL; // Handle error: Could not create pattern
  }
  /* FcPatternAddString(pattern, FC_FAMILY, (const FcChar8*)face_name); */
  FcConfigSubstitute(config, pattern, FcMatchPattern);
  FcDefaultSubstitute(pattern);
  FcObjectSet* os = FcObjectSetBuild(FC_FAMILY, FC_STYLE, FC_FILE, (char*)0);
  // FcFontSet* fs = FcFontSetCreate();
  FcResult result;
  FcFontSet* fs = FcFontSort(config, pattern, FcTrue, 0, &result);
  if (fs == NULL || fs->nfont == 0){
    fprintf(stderr, "Fontconfig could not find ANY fonts on the system?\n");
    return NULL;  // A better return code is better.
  }

  /* FcFontRenderPrepare(config, patttern,  */

  FcPattern* matches = FcFontMatch(config, pattern, &result);
  fprintf(stderr, "FcFontMatch result: %d\n", result);
  return NULL;

  /* FcObjectSet* os = FcObjectSetCreate(); */
  /* if (!os) { */
  /*   FcPatternDestroy(pattern); */
  /*   return NULL; // Handle error */
  /* } */
  /* FcObjectSetAdd(os, FC_FILE); // We want the filename */


  /* FcFontSet* fs = FcFontSetMatch(config, NULL, 0, pattern, &result); // Correct usage */
  /* if (!fs || result != FcResultMatch) {  // Check result! */
  /*   FcPatternDestroy(pattern); */
  /*   FcObjectSetDestroy(os); */
  /*   if (fs) { */
  /*     FcFontSetDestroy(fs); // Destroy if it was created */
  /*   } */
  /*   fprintf(stderr, "Error: No matching fonts found (or other Fontconfig error): FcResult=%d\n", result); */
  /*   return NULL; */
  /* } */

  /* FcFontSet* fs = FcFontSetMatch(config, pattern, os);  // Find matching fonts */
  /* if (!fs) { */
  /*   FcPatternDestroy(pattern); */
  /*   FcObjectSetDestroy(os); */
  /*   return NULL; // Handle error: No matching fonts found */
  /* } */

  /* FT_Face* face = NULL; */
  /* if (fs->nFont > 0) { */
  /*   FcChar8* filename = NULL; */
  /*   FcPattern *font_pattern = FcFontRenderPrepare(config, pattern, fs->fonts[0]); */
  /*   if (font_pattern) { */
  /*     if (FcPatternGetString(font_pattern, FC_FILE, 0, &filename) == FcResultMatch && filename) { */
  /*       if (FT_New_Face(ft, (const char*)filename, 0, &face) != 0) { */
  /*         // Handle FreeType error (e.g., invalid font file) */
  /*         face = NULL; // Important: Set face to null on error */
  /*       } */
  /*     } */
  /*     FcPatternDestroy(font_pattern); */
  /*   } */
  /* } */

  /* FcFontSetDestroy(fs); */
  /* FcPatternDestroy(pattern); */
  /* FcObjectSetDestroy(os); */

  return ft_face;
}

int main() {
  FT_Library ft;
  if (FT_Init_FreeType(&ft)) {
    fprintf(stderr, "Error: FreeType initialization failed\n");
    return 1;
  }
  /* const char* face_name = "Arial"; // Example font name (replace with your font) */
  const char* face_path =
      "/home/honar/.fonts/Spline Sans Mono/static/SplineSansMono-Regular.ttf";
  FT_Face ft_face = NULL;
  if (FT_New_Face(ft, face_path, 0, &ft_face)) {
    fprintf(stderr, "Failed to load typeface from path %s\n", face_path);
  }
  /* FT_Face face = ObtainFace(ft, face_name); */
  /* if (!face) { */
  /*   fprintf(stderr, "Error: Could not obtain face: %s\n", face_name); */
  /*   FT_Done_FreeType(ft); */
  /*   return 1; */
  /* } */

  hb_font_t* hb_font = hb_ft_font_create(ft_face, NULL);
  Display* display = XOpenDisplay(NULL);
  if (!display) {
    fprintf(stderr, "Error: Could not open X display\n");
    hb_font_destroy(hb_font);
    FT_Done_Face(ft_face);
    FT_Done_FreeType(ft);
    return 1;
  }

  int screen = DefaultScreen(display);
  Window window = RootWindow(display, screen);
  GC gc = XCreateGC(display, window, 0, NULL);
  if (!gc) {
    fprintf(stderr, "Error: Could not create GC\n");
    XCloseDisplay(display);
    hb_font_destroy(hb_font);
    FT_Done_Face(ft_face);
    FT_Done_FreeType(ft);
    return 1;
  }

  Evas_Engine_Info* einfo;
  Evas* evas = evas_new();
  if (!evas) {
    fprintf(stderr, "Evas creation failed!\n");
    ecore_shutdown();
    return 1;
  }

  // Get engine info (important for setting up the canvas)
  einfo = (Evas_Engine_Info *)evas_engine_info_get(evas);
  if (!einfo) {
    fprintf(stderr, "Failed to get Evas engine info!\n");
    evas_free(evas);
    ecore_shutdown();
    return 1;
  }

  Ecore_Evas *ee = ecore_evas_new(NULL, 0, 0, 800, 600); // Adjust size as needed
  if (!ee) {
    fprintf(stderr, "Ecore_Evas creation failed!\n");
    evas_free(evas);
    ecore_shutdown();
    return 1;
  }
  ecore_evas_title_set(ee, "HarfBuzz with EFL");
  ecore_evas_show(ee);

  evas_object_show(ecore_evas_get(ee)); // Show the underlying Evas canvas

  // Set the evas on the engine info.
  einfo->info.screen.w = 800; // Set width and height
  einfo->info.screen.h = 600;
  evas_engine_info_set(evas, einfo);

  const char* text = "Hello, HarfBuzz/FreeType/X11! مرحبا"; // Example with Arabic
  /* int x = 100; */
  /* int y = 100; */

  hb_buffer_t* buffer = hb_buffer_create();
  hb_buffer_add_utf8(buffer, text, strlen(text), 0, -1);
  hb_buffer_set_direction(buffer, HB_DIRECTION_LTR);
  hb_buffer_set_script(buffer, HB_SCRIPT_LATIN);
  hb_buffer_set_language(buffer, hb_language_from_string("en", -1));

  hb_shape(hb_font, buffer, NULL, 0);

  unsigned int glyph_count = hb_buffer_get_length(buffer);
  hb_glyph_info_t* glyph_infos = hb_buffer_get_glyph_infos(buffer, NULL);
  hb_glyph_position_t* glyph_positions = hb_buffer_get_glyph_positions(buffer, NULL);

  FT_Set_Pixel_Sizes(ft_face, 0, 16);

  // EFL rendering
  Evas_Object *text_object;
  int x = 100, y = 100; // Starting position

  for (unsigned int i = 0; i < glyph_count; ++i) {
    FT_UInt glyph_index = glyph_infos[i].codepoint; // Get glyph index

    // Load glyph from FreeType
    if (FT_Load_Glyph(ft_face, glyph_index, FT_LOAD_DEFAULT)) {
      // Handle error
    }

    if (FT_Render_Glyph(ft_face->glyph, FT_RENDER_MODE_NORMAL)) {
      // Handle error
    }

    FT_Bitmap *bitmap = &ft_face->glyph->bitmap;

    // Create Evas image object for the glyph
    text_object = evas_object_image_add(evas);
    evas_object_image_size_set(text_object, bitmap->width, bitmap->rows);
    evas_object_image_data_set(text_object, bitmap->buffer); // Set pixel data
    evas_object_move(text_object,
                     x + glyph_pos[i].x_offset / 64.0,
                     y + glyph_pos[i].y_offset / 64.0); // Position the glyph
    evas_object_show(text_object);
    // Move x for the next glyph (advance)
    x += glyph_pos[i].x_advance / 64.0;

  }


  /* int current_x = x; */
  /* for (unsigned int i = 0; i < glyph_count; ++i) { */
  /*   FT_Load_Glyph(face, glyph_infos[i].codepoint, FT_LOAD_DEFAULT); */
  /*   FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL); // Render the glyph */

  /*   // Draw the glyph bitmap to the X11 window: */
  /*   FT_Bitmap* bitmap = &face->glyph->bitmap; */
  /*   for (int j = 0; j < bitmap->rows; ++j) { */
  /*     for (int k = 0; k < bitmap->width; ++k) { */
  /*       if (bitmap->buffer && bitmap->buffer[j * bitmap->pitch + k]) { // Check for set pixel */
  /*         XDrawPoint(display, window, gc, */
  /*                    current_x + glyph_positions[i].x_offset / 64 + k, */
  /*                    y + glyph_positions[i].y_offset / 64 + face->glyph->bitmap_top + j); */
  /*       } */
  /*     } */
  /*   } */
  /*   current_x += glyph_positions[i].x_advance / 64; */
  /* } */


  /* XFontStruct* xfont_struct = XLoadFont(display, XftFontGetName(face)); */
  /* if(xfont_struct) */
  /*     XSetFont(display, gc, xfont_struct->fid); */
  /* else{ */
  /*     fprintf(stderr, "Error: Could not set font in GC\n"); */
  /*     XFreeGC(display, gc); */
  /*     XCloseDisplay(display); */
  /*     hb_buffer_destroy(buffer); */
  /*     hb_font_destroy(hb_font); */
  /*     FT_Done_Face(face); */
  /*     FT_Done_FreeType(ft); */
  /*     return 1; */
  /* } */

  /* int current_x = x; */
  /* for (unsigned int i = 0; i < glyph_count; ++i) { */
  /*     FT_Load_Glyph(face, glyph_infos[i].codepoint, FT_LOAD_DEFAULT); */
  /*     FT_Render_Glyph(face, FT_RENDER_MODE_NORMAL); */

  /*     XSetFont(display, gc, xfont_struct->fid); */
  /*     XDrawString(display, window, gc, current_x + glyph_positions[i].x_offset / 64, */
  /*                   y + glyph_positions[i].y_offset / 64 + face->glyph->bitmap_top, */
  /*                   (const char*)&face->glyph->bitmap.buffer[0], face->glyph->bitmap.width); */

  /*     current_x += glyph_positions[i].x_advance / 64; */
  /* } */

  hb_buffer_destroy(buffer);
  hb_font_destroy(hb_font);
  /* XFreeFont(display, xfont_struct); */
  XFreeGC(display, gc);
  XCloseDisplay(display);
  FT_Done_Face(ft_face);
  FT_Done_FreeType(ft);

  return 0;
}

// cc -o $tmpdir/a eina_array_example.c `pkg-config --cflags --libs eina`
#include <stdio.h>
#include <Eina.h>

#include <err.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/un.h>
#include <unistd.h>

int main() {
  struct sockaddr_un addr;
  printf("sizeof(addr.sun_path): %d\n", sizeof(addr.sun_path));
  eina_init(); // Initialize Eina
  Eina_Array* array = eina_array_new(16); // Initial capacity of 16
  char data_to_add[] = "Hello";
  for (size_t i = 0; data_to_add[i] != '\0'; ++i) {
    eina_array_push(array, &data_to_add[i]); // Add each character
  }
  char* item;
  int i;
  Eina_Array_Iterator iterator;
  EINA_ARRAY_ITER_NEXT(array, i, item, iterator) {
    printf("%c", *item);
  }
  printf("\n");
  // ... add more data as needed ...
  eina_array_free(array); // Free the array (important!)
  eina_shutdown(); // Shutdown Eina
  return 0;
}

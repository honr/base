package pie

import (
	"fmt"
	"io/fs"
	"os"
	"os/exec"
	"path"
	"strings"
)

var (
	kZooDir                           = "$HOME/Zoo"
	kRequirementsFilename             = "req.txt"
	kDefaultDirMode       fs.FileMode = 0700
	kDefaultFileMode      fs.FileMode = 0600
)

func Dispatch(verb string, args []string) error {
	switch verb {
	case ".ls":
		return Ls(args)
	case ".create":
		return Create(args)
	case ".clear":
		return Clear(args)
	case ".remove":
		return Remove(args)
	case ".edit":
		return Edit(args)
	case ".update":
		return Update(args)
	case ".freeze":
		return Freeze(args)
	default:
		if len(args) < 1 || args[0] == "" {
			return fmt.Errorf("Must specify the cage.")
		}
		cage := trimCage(args[0])
		cmdArgs := append([]string{"-m", verb}, args[1:]...)
		cmd := exec.Command("bin/python3", cmdArgs...)
		cmd.Dir = cageDirectory(cage)
		cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
		return cmd.Run()
	}
}

func trimCage(cageMaybe string) string {
	if strings.HasPrefix(cageMaybe, "@") {
		return strings.TrimPrefix(cageMaybe, "@")
	}
	return cageMaybe
}

func cageDirectory(cage string) string {
	return path.Join(os.ExpandEnv(kZooDir), cage)
}

func Ls(args []string) error {
	dir := path.Dir(cageDirectory("dummy"))
	des, err := os.ReadDir(dir)
	if err != nil {
		return err
	}
	for _, de := range des {
		if !de.IsDir() {
			continue
		}
		fmt.Printf("%s\n", de.Name())
	}
	return nil
}

func Create(args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("Too few args.  Need at least one.")
	}
	cage := trimCage(args[0])
	dir := cageDirectory(cage)
	// Don't overwrite existing.
	_, err := os.Stat(dir)
	if err == nil {
		return fmt.Errorf("Path already exists: %s", dir)
	}
	if !os.IsNotExist(err) {
		return fmt.Errorf("Path already exists: %s; stat error: %s", dir, err)
	}
	parentDir := path.Dir(dir)
	os.MkdirAll(parentDir, kDefaultDirMode)
	cmd := exec.Command("python3", "-m", "venv", "--without-pip", "--system-site-package", cage)
	cmd.Dir = parentDir
	cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
	return cmd.Run()
}

func Clear(args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("Too few arguments.  Need at least one.")
	}
	cage := trimCage(args[0])
	if cage == "" {
		return fmt.Errorf("Directory name empty.")
	}
	dir := cageDirectory(cage)
	fi, err := os.Stat(dir)
	if err != nil {
		return fmt.Errorf("Failed to stat(1) cage directory %s.  ERR %s", dir, err)
	}
	if !fi.IsDir() {
		return fmt.Errorf("Cage (%s) is not a directory", dir)
	}
	for _, subdir := range []string{
		"bin",
		"etc",
		"include",
		"lib",
		"share",
		"pyvenv.cfg",
	} {
		os.RemoveAll(path.Join(dir, subdir))
	}
	return nil
}

func Remove(args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("Too few arguments.  Need at least one.")
	}
	cage := trimCage(args[0])
	if cage == "" {
		return fmt.Errorf("Directory name empty.")
	}
	dir := cageDirectory(cage)
	fmt.Fprintf(os.Stderr, "Deleting directory and its contents: %s\n", dir)
	return os.RemoveAll(dir)
}

func Edit(args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("Too few arguments.  Need at least one.")
	}
	cage := trimCage(args[0])
	if cage == "" {
		return fmt.Errorf("Directory name empty.")
	}
	dir := cageDirectory(cage)
	cmd := exec.Command("emacsclient", "-nc",
		path.Join(dir, kRequirementsFilename))
	cmd.Dir = dir
	cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
	return cmd.Run()
}

func Update(args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("Too few arguments.  Need at least one.")
	}
	cage := trimCage(args[0])
	if cage == "" {
		return fmt.Errorf("Directory name empty.")
	}
	dir := cageDirectory(cage)
	cmd := exec.Command("./bin/python3", "-m", "pip",
		"install", "--upgrade", "-r",
		path.Join(dir, kRequirementsFilename))
	cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
	cmd.Dir = dir
	return cmd.Run()
}

func Freeze(args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("Too few arguments.  Need at least one.")
	}
	cage := trimCage(args[0])
	if cage == "" {
		return fmt.Errorf("Directory name empty.")
	}
	dir := cageDirectory(cage)
	f, err := os.OpenFile(path.Join(dir, kRequirementsFilename),
		os.O_WRONLY|os.O_CREATE, kDefaultFileMode)
	if err != nil {
		return err
	}
	defer f.Close()
	cmd := exec.Command("bin/python3", "-m", "pip", "freeze", "--local",
		"--requirement", path.Join(dir, kRequirementsFilename))
	cmd.Stdin, cmd.Stderr = os.Stdin, os.Stderr
	cmd.Stdout = f
	cmd.Dir = dir
	return cmd.Run()
}

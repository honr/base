package topicaltmux

import (
	"fmt"
	"honr.gitlab.com/base/topical"
	"os"
	"os/exec"
	"path"
)

// ListSessions list current sessions.
func ListSessions() error {
	cmd := exec.Command("tmux", "list-sessions")
	cmd.Stdout = os.Stdout
	return cmd.Run()
}

// Attach creates a session, if needed, and attaches to it.
func Attach(topic string, configDir string) error {
	// Prefixing a session name with '=' makes it match the exact string.
	cmd := exec.Command("tmux", "attach-session", "-t", "="+topic)
	cmd.Stdin, cmd.Stderr, cmd.Stdout = os.Stdin, os.Stderr, os.Stdout
	if err := cmd.Run(); err == nil {
		return nil // Success.
	}

	topicInfo, err := topical.Get(topic)
	if err != nil {
		return err
	}
	dir := topicInfo.Dir
	// TODO: Introduce a way to set Env vars for a particular session.  E.g.,
	// $QUEST.env
	cmd = exec.Command("tmux", "new-session", "-d", "-s", topic, "-c", dir, "env",
		fmt.Sprintf("QUEST=%s", topic), os.ExpandEnv("$SHELL"))
	cmd.Stderr, cmd.Stdout = os.Stderr, os.Stdout
	if err = cmd.Run(); err != nil {
		return err
	}

	cmd = exec.Command("tmux", "set-environment", "-t", topic, "QUEST", topic)
	cmd.Stderr, cmd.Stdout = os.Stderr, os.Stdout
	if err = cmd.Run(); err != nil {
		return err
	}

	// TODO: source ~/T/$QUEST
	topicInitFile := path.Join(configDir, topic) + ".rc"
	if _, err := os.Stat(topicInitFile); err == nil {
		cmd = exec.Command("sh", topicInitFile)
		cmd.Stdin, cmd.Stderr, cmd.Stdout = os.Stdin, os.Stderr, os.Stdout
		if err = cmd.Run(); err != nil {
			return err
		}
	}

	cmd = exec.Command("tmux", "attach-session", "-t", topic)
	cmd.Stdin, cmd.Stderr, cmd.Stdout = os.Stdin, os.Stderr, os.Stdout
	if err = cmd.Run(); err != nil {
		return err
	}
	return nil
}

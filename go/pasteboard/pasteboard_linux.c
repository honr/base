//go:build linux

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

int pasteboardRead(void** buf) {
  Display *display = XOpenDisplay(NULL);
  if (!display) {
    return 0;
  }
  Window root = DefaultRootWindow(display);
  Atom clipboardAtom = XInternAtom(display, "CLIPBOARD", False);
  Atom utf8Atom = XInternAtom(display, "UTF8_STRING", False);
  Atom actual_type_return;
  int actual_format_return; // format of the property (16, 32, or 8 bits).
  uint64_t length = 0;
  uint64_t bytes_after_return = 0;
  unsigned char* data = NULL;
  int success = XGetWindowProperty(
      display, root, clipboardAtom, 0L, 0L, False,
      utf8Atom, &actual_type_return, &actual_format_return,
      &length, &bytes_after_return, &data);
  if (success == Success && length > 0) {
    if (actual_type_return == utf8Atom) {
      *buf = malloc(length + 1);
      if (*buf) {
        memcpy(*buf, data, length);
        *((char*)*buf + length) = '\0';
      } else {
        length = 0; // Allocation failed
      }
    }
    XFree(data); // Free the data, regardless of type match
  }
  XCloseDisplay(display);
  return (int) length;
}

// This is broken.  It needs to hang around and listen to events.
int pasteboardWrite(char* buf, int size) {
  Display *display = XOpenDisplay(NULL);
  if (!display) {
    return 0; // Failed to open display
  }
  Window root = DefaultRootWindow(display);
  Atom clipboardAtom = XInternAtom(display, "CLIPBOARD", False);
  Atom utf8Atom = XInternAtom(display, "UTF8_STRING", False);
  if (clipboardAtom == None || utf8Atom == None) {
    XCloseDisplay(display);
    return 0; // Failed to get atoms
  }
  char* data;
  if (buf[size - 1] == '\0') { // Check if already null-terminated
    data = buf;
  } else {
    data = (char*)malloc(size + 1);
    if (!data) {
      XCloseDisplay(display);
      return 0;
    }
    memcpy(data, buf, size);
    data[size] = '\0';
  }
  Window owner = root;
  XChangeProperty(display, owner, clipboardAtom, utf8Atom, 8,
                  PropModeReplace, (unsigned char*)data, size);
  XSetSelectionOwner(display, clipboardAtom, owner, CurrentTime);
  if (owner != XGetSelectionOwner(display, clipboardAtom)) {
    fprintf(stderr, "error: Failed to take ownership of selection.\n");
    return 0;
  }
  XFlush(display);
  XCloseDisplay(display);
  if (data != buf) { // Free only if we allocated memory
    free(data);
  }
  return size;
}

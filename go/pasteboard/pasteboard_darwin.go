//go:build darwin

package pasteboard

/*
#cgo CFLAGS: -x objective-c
#cgo LDFLAGS: -framework Foundation -framework Cocoa
#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

int pasteboardRead(void **buf);
int pasteboardWrite(const void *buf, NSInteger n);
*/
import "C"
import (
	"errors"
	"unsafe"
)

func Read(p []byte) (int, error) {
	var data unsafe.Pointer
	n := int(C.pasteboardRead(&data))
 	if data == nil || n == 0 {
		return 0, errors.New("Failed to read pasteboard")
	}
	defer C.free(unsafe.Pointer(data))
	copy(p, C.GoBytes(data, C.int(n)))
	return n, nil
}

func Write(p []byte) (int, error) {
	var ok C.int
	n := len(p)
	if n == 0 {
		return 0, nil // Silently refuse to write an empty slice.
	}
	if ok = C.pasteboardWrite(unsafe.Pointer(&p[0]), C.NSInteger(n)); ok != 0 {
		return 0, errors.New("Failed to write to pasteboard")
	}
	return n, nil
}

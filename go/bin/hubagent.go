package main

// TODO: Turn hub into hubagent and hubclient.  Start hubagent via systemd (or
// launchd).

import (
	"fmt"
	"net"
	"net/rpc"
	"os"
	"path"
	"time"

	"honr.gitlab.com/base/agent"
)

var (
	kSocketPath = "${HOME}/.ssh/hubagent"
)

type Handler struct{}

// TODO: Implement.
func (h *Handler) ReloadConfig(in agent.ReloadConfigI,
	out *agent.ReloadConfigO) error {
	out.Err = nil
	return nil
}

type Conn struct {
	Listener net.Listener
}

func (c *Conn) Close() {
	c.Listener.Close()
}

func Dial(socketPath string) (*Conn, error) {
	if _, err := os.Stat(socketPath); err == nil || os.IsExist(err) {
		os.Remove(socketPath)
	}
	os.MkdirAll(path.Dir(socketPath), 0700)
	l, _ := net.Listen("unix", socketPath)
	if err := os.Chmod(socketPath, 0700); err != nil {
		return nil, err
	}
	return &Conn{Listener: l}, nil
}

func main() {
	startTime := time.Now()
	socketPath := os.ExpandEnv(kSocketPath)
	rpc.Register(&Handler{})
	conn, err := Dial(socketPath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(2)
	}
	defer conn.Close()
	fmt.Fprintf(os.Stderr, "Started on %v,\nlistening on '%s'.\n",
		startTime, socketPath)
	rpc.Accept(conn.Listener)
}

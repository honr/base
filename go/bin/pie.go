package main

import (
	"log"
	"os"

	"honr.gitlab.com/base/pie"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Too few arguments.  At least one verb is needed")
	}
	verb := os.Args[1]
	if err := pie.Dispatch(verb, os.Args[2:]); err != nil {
		log.Fatal(err)
	}
}

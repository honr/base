package main

import (
	"log"
	"os"

	"honr.gitlab.com/base/topical"
	"honr.gitlab.com/base/topical/topicalscreen"
)

func main() {
	if len(os.Args) < 2 {
		topicalscreen.ListSessions() // ignore errors.
		return
	}
	topic := os.Args[1]
	if topic == "@" {
		topic = os.Getenv("QUEST")
	}
	if topic == "" {
		log.Fatalf("$QUEST not set.")
	}
	screenAttachArgs := os.Args[2:]
	dir := topical.TopicsDir
	if err := topicalscreen.Attach(topic, dir, screenAttachArgs); err != nil {
		log.Fatal(err)
	}
	return
}

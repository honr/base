package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"text/template"

	i3 "go.i3wm.org/i3/v4"
	"honr.gitlab.com/base/topical"
	"honr.gitlab.com/base/topical/topicalscreen"
)

type DEClass int

const (
	deUNKNOWN DEClass = iota
	deI3
	deSway
	deGnome
	deOtherLinux
	deMacos
)

type DE struct {
	Class DEClass
}

func (de *DE) LikeI3() bool {
	return de.Class == deI3 || de.Class == deSway
}

func (de *DE) IsMacos() bool {
	return de.Class == deMacos
}

var (
	kHome            string
	kChromeProgram       = "google-chrome"
	kTerminalProgram     = "alacritty"
	kVivaldiUsersDir     = "$HOME/.config/viv"
	kDmenuProgram        = "rofi"
	kDE              *DE = nil

	kWirePlumberVolumeRE = regexp.MustCompile(`([-+0-9.]+)(%)?([-+])?`)
	// We only care about the first two components of the version.
	kAlacrittyVersionRE  = regexp.MustCompile(`alacritty ([0-9]*\.[0-9]+)`)
	kAlacrittyConfigTmpl = template.Must(template.New("list").Parse(
		`{{.Import}} = [
      "{{.Home}}/Pool/base/etc/alacritty-common.toml",
      "{{.Home}}/Pool/base/etc/alacritty-{{.Theme}}.toml",
    ]`))
)

func init() {
	kHome = os.Getenv("HOME")
	if program := os.Getenv("DEFAULT_CHROME"); program != "" {
		kChromeProgram = program
	}
	if program := os.Getenv("DEFAULT_TERMINAL"); program != "" {
		kTerminalProgram = program
	}
	if program := os.Getenv("DMENU"); program != "" {
		kDmenuProgram = program
	}
	if runtime.GOOS == "darwin" {
		kDE = &DE{Class: deMacos}
		return
	}
	switch os.Getenv("XDG_SESSION_DESKTOP") {
	case "i3":
		i3SockPath := os.Getenv("I3SOCK")
		if i3SockPath != "" {
			kDE = &DE{Class: deI3}
			i3.SocketPathHook = func() (string, error) {
				return i3SockPath, nil
			}
		}
	case "sway":
		swaySockPath := os.Getenv("SWAYSOCK")
		if swaySockPath != "" {
			kDE = &DE{Class: deSway}
			i3.SocketPathHook = func() (string, error) {
				return swaySockPath, nil
			}
		}
	case "gnome":
		kDE = &DE{Class: deGnome}
	default:
		kDE = &DE{Class: deOtherLinux}
	}
}

// TODO: Call a hub agent that we start with systemd (or launchd) to show a menu
// and make a selection.
func dmenuSelect(prompt string, choices []string) (choice string, err error) {
	var cmd *exec.Cmd
	if os.Getenv("XDG_SESSION_TYPE") == "wayland" {
		// Note: none of these seems to work well enough.  Keeping the configs in
		// case one has a regression.
		switch kDmenuProgram {
		case "fuzzle":
			// Too big.  Don't like it.
			cmd = exec.Command("fuzzel", "--dmenu", "--prompt", prompt+" ")
		case "tofi":
			// Seems a bit bigger than others.  Perhaps unnecessarily, so.
			// - Bad default keybindings.
			cmd = exec.Command("tofi",
				"--width", "100%",
				"--anchor", "top",
				"--prompt-text", prompt+" ")
		case "wofi":
			// Small, but don't like it much.
			// - Centred.
			// - Bad bindings.
			cmd = exec.Command("wofi", "--show", "dmenu", "-i", "-p", prompt)
		case "bemenu":
			// - Keybindings only mostly okay. Vim bindings are available and a bit
			//   better.
			cmd = exec.Command("bemenu", "-i", "-p", prompt) // index:quoted choice
		case "wmenu":
			// Doesn't seem to work properly with wayland.  Couldn't get the text to
			// show less fuzzy.
			// - Keybindings seem reasonable.
			cmd = exec.Command("wmenu", "-i", // "-o", "eDP-1",
				"-N", "111111ff", "-M", "111111ff", "-S", "1a1a1aff",
				"-n", "666666ff", "-m", "666666ff", "-s", "ffffffff",
				"-f", "RobotoCondensed 10",
				"-p", prompt+" ")
		case "rofi", "default":
			// Pretty small.
			// - Not covering the wayland top bar.
			// - Configurable bindings.
			cmd = exec.Command("rofi", "-dmenu", "-i", "-p", prompt)
		}
	} else {
		// index:quoted choice
		cmd = exec.Command("rofi", "-dmenu", "-dpi", "1", "-i", "-p", prompt)
	}
	var out bytes.Buffer
	cmd.Stdin = strings.NewReader(strings.Join(choices, "\n"))
	cmd.Stdout = &out
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return "", err
	}
	choice = strings.TrimSpace(string(out.Bytes()))
	return choice, nil
}

func chromiumCmd(browser string, subdir string, incognito bool, otherArgs []string) (*exec.Cmd, error) {
	if subdir == "" {
		subdir = "default/Default"
	}
	dataDir, profileName, _ := strings.Cut(subdir, "/")
	if incognito {
		otherArgs = append(otherArgs, "--incognito")
	}
	browserUsersDir := "/dev/null"
	switch fmt.Sprintf("%s:%s", runtime.GOOS, browser) {
	case "darwin:chrome":
		browser = "Google Chrome"
		browserUsersDir = "${HOME}/Library/Application Support/Google Chrome"
	case "darwin:vivaldi":
		browser = "Vivaldi"
		browserUsersDir = "${HOME}/Library/Application Support/Vivaldi"
	case "linux:chrome":
		browser = kChromeProgram
		browserUsersDir = topical.ChromeUsersDir
	case "linux:vivaldi":
		browser = "vivaldi"
		browserUsersDir = kVivaldiUsersDir
	}
	if runtime.GOOS == "darwin" {
		return exec.Command("open",
			append([]string{"-a", browser, "--"}, otherArgs...)...), nil
	}
	args := []string{"--no-first-run", "--no-default-browser-check"}
	if dataDir != "" {
		args = append(args, fmt.Sprintf("--user-data-dir=%s",
			filepath.Join(os.ExpandEnv(browserUsersDir), dataDir)))
	}
	if profileName != "" {
		args = append(args, fmt.Sprintf("--profile-directory=%s", profileName))
	}
	// For wayland related issues, either use the following extra args:
	//   "--enable-features=UseOzonePlatform",
	//   "--ozone-platform=wayland",
	// or set the desired value for the flag:
	//   chrome://flags#ozone-platform-hint (or vivaldi://flags#ozone-platform-hint).
	args = append(args, "--new-window")
	args = append(args, otherArgs...)
	return exec.Command(browser, args...), nil
}

// Here are some ways to find list of browser profiles.  Perhaps they can be
// exposed via a subcommand (e.g., hub browser-profiles):
// ls .config/chrome/*/*/Preferences
// ls .config/viv/*/*/Preferences
// ls .config/vivaldi/*/Preferences
// ls .config/google-chrome/*/Preferences
func runBrowser(profile string, incognito bool, args []string) (err error) {
	browser, subdir, _ := strings.Cut(profile, "/")
	var cmd *exec.Cmd
	switch browser {
	case "cr":
		fallthrough
	case "chrome":
		if cmd, err = chromiumCmd("chrome", subdir, incognito, args); err != nil {
			return err
		}
	case "viv":
		fallthrough
	case "vivaldi":
		if cmd, err = chromiumCmd("vivaldi", subdir, incognito, args); err != nil {
			return err
		}
	case "fire":
		fallthrough
	case "firefox":
		if incognito {
			args = append(args, "--private-window")
		}
		cmd = exec.Command("firefox", append(args, "-P", subdir)...)
	default:
		return fmt.Errorf("Do not know how to run browser, subdir: %q, %q", browser, subdir)
	}
	cmd.Stdin, cmd.Stdout, cmd.Stderr = nil, nil, os.Stderr
	return cmd.Run()
}

func focusedWorkspace() (*i3.Node, error) {
	tree, err := i3.GetTree()
	if err != nil {
		return nil, err
	}
	ws := tree.Root.FindFocused(func(n *i3.Node) bool {
		return n.Type == i3.WorkspaceNode
	})
	if ws == nil {
		return nil, fmt.Errorf("could not locate workspace")
	}
	return ws, nil
}

func focusedTopic() (string, error) {
	if !kDE.LikeI3() {
		return "default", nil
	}
	ws, err := focusedWorkspace()
	if err != nil {
		return "", err
	}
	name := ws.Name
	// foo.bar.baz -> foo
	if i := strings.Index(name, "."); i > 0 {
		return name[:i], nil
	}
	return name, nil
}

func focusedTopicDir() (string, error) {
	topic, err := focusedTopic()
	if err != nil {
		return "", err
	}
	topicInfo, err := topical.Get(topic)
	if err != nil {
		return "", err
	}
	return topicInfo.Dir, nil
}

func focusedBrowserProfile(topic string) (string, error) {
	topicInfo, err := topical.Get(topic)
	if err != nil {
		return "", err
	}
	return topicInfo.ChromeProfile, nil
}

func allTopics(args []string) ([]string, error) {
	topics, err := i3.GetWorkspaces()
	if err != nil {
		return nil, err
	}
	topicNames := make([]string, len(topics))
	for i, topic := range topics {
		topicNames[i] = topic.Name
	}
	return topicNames, nil
}

func allMarks(arg []string) ([]string, error) {
	marks, err := i3.GetMarks()
	if err != nil {
		return nil, err
	}
	return marks, nil
}

func potentialTopics(args []string) ([]string, error) {
	return topical.List(), nil
}

func i3status(args []string) (string, error) {
	recv := i3.Subscribe(i3.WindowEventType)
	otherStatus := ""
	windowTitle := ""
	for recv.Next() {
		ev := recv.Event()
		switch v := ev.(type) {
		case *i3.WindowEvent:
			if v.Change == "focus" || v.Change == "new" || v.Change == "title" {
				windowTitle = v.Container.Name
			}
		}
		fmt.Printf("|%s|%s\n", otherStatus, windowTitle)
	}
	log.Fatal(recv.Close())
	return "", nil
}

func listen(args []string) (string, error) {
	recv := i3.Subscribe(i3.BindingEventType)
	for recv.Next() {
		ev := recv.Event()
		switch v := ev.(type) {
		case *i3.BindingEvent:
			b := v.Binding
			fmt.Printf("  - %s\n", b.Command)
		default:
			fmt.Printf("  - I didn't subscribe to this type - value: %v", v)
		}
	}
	log.Fatal(recv.Close())
	return "", nil
}

func doTerm(args []string) error {
	cmd := exec.Command(kTerminalProgram, args...)
	topic, _ := focusedTopic()
	topicDir, _ := focusedTopicDir()
	if topic != "" {
		cmd.Env = append(os.Environ(),
			fmt.Sprintf("QUEST=%s", topic),
			fmt.Sprintf("QUESTDIR=%s", topicDir),
			fmt.Sprintf("QUESTSDIR=%s", topical.TopicsDir))
		if _, err := os.Stat(topicDir); err == nil {
			cmd.Dir = topicDir
		}
	}
	cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
	return cmd.Run()
}

func doTermScreen(args []string) error {
	topic, err := focusedTopic()
	if err != nil {
		return err
	}
	cmd := exec.Command(kTerminalProgram, "-e", ".local/bin/t", topic)
	cmd.Env = append(os.Environ(), fmt.Sprintf("QUEST=%s", topic))
	cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
	return cmd.Run()
}

func doAudio(args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("Needs an argument")
	}
	action := args[0]
	// With ALSA here are some examples:
	//   amixer -q sset Master,0 1+ unmute
	//   amixer -q sset Master,0 1- unmute
	//   amixer -D pulse set Master toggle
	// Example with pulseaudio:
	//   pactl set-sink-volume @DEFAULT_SINK@ '30%'
	// Example with wireplumber (to handle pipewire):
	//   wpctl set-volume @DEFAULT_AUDIO_SINK@ '30%'
	var cmd *exec.Cmd
	if kWirePlumberVolumeRE.MatchString(action) {
		cmd = exec.Command("wpctl", "set-volume", "@DEFAULT_AUDIO_SINK@", action)
	} else if action == "mute-toggle" || action == "m" {
		cmd = exec.Command("wpctl", "set-mute", "@DEFAULT_AUDIO_SINK@", "toggle")
	} else if action == "mute" {
		cmd = exec.Command("wpctl", "set-mute", "@DEFAULT_AUDIO_SINK@", "1")
	} else if action == "unmute" {
		cmd = exec.Command("wpctl", "set-mute", "@DEFAULT_AUDIO_SINK@", "0")
	} else if action == "increase" || action == "+" {
		cmd = exec.Command("wpctl", "set-volume", "@DEFAULT_AUDIO_SINK@", "2%+")
	} else if action == "decrease" || action == "-" {
		cmd = exec.Command("wpctl", "set-volume", "@DEFAULT_AUDIO_SINK@", "2%-")
	} else {
		return fmt.Errorf("I don't recognize the action '%s'", action)
	}
	cmd.Stdin, cmd.Stdout, cmd.Stderr = nil, nil, os.Stderr
	return cmd.Run()
}

func doBrightness(args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("Needs an argument")
	}
	action := args[0]
	sysBrightness := "/sys/class/backlight/intel_backlight/brightness"
	sysMaxBrightness := "/sys/class/backlight/intel_backlight/max_brightness"
	maxBrightness := 500
	brightness := 100
	{
		f, err := os.OpenFile(sysMaxBrightness, os.O_RDONLY, 0o666)
		defer f.Close()
		if err != nil {
			return err
		}
		if _, err = fmt.Fscanf(f, "%d", &maxBrightness); err != nil {
			return err
		}
	}
	f, err := os.OpenFile(sysBrightness, os.O_RDWR, 0o666)
	defer f.Close()
	if err != nil {
		return err
	}
	if _, err = fmt.Fscanf(f, "%d", &brightness); err != nil {
		return err
	}
	if _, err = f.Seek(0, 0); err != nil {
		return err
	}
	if action == "increase" {
		// (exp (/ (log 504) 32)): 1.21464944
		brightness = min(int(1+float64(brightness)*1.21), maxBrightness)
		fmt.Fprintf(f, "%d", brightness)
	}
	if action == "decrease" {
		brightness = max(int(0.82*float64(brightness-1)), 0)
		fmt.Fprintf(f, "%d", brightness)
	}
	return nil
}

func doBrowse(incognito bool, args []string) error {
	if incognito {
		profile := "vivaldi/wild"
		return runBrowser(profile /*incognito*/, true, args)
	}
	topic, err := focusedTopic()
	if err != nil {
		return err
	}
	profile, err := focusedBrowserProfile(topic)
	if err != nil {
		return err
	}
	return runBrowser(profile /*incognito*/, false, args)
}

func doBrowserPick(args []string) error {
	profiles := []string{}
	if files, err := os.ReadDir(topical.ChromeUsersDir); err == nil {
		profiles = make([]string, len(files))
		for i, profileDir := range files {
			profiles[i] = "chrome/" + profileDir.Name()
		}
	}
	profiles = append(profiles, "firefox/default")
	profiles = append(profiles, "vivaldi/default")
	prompt := "Browser"
	isIncognito := false
	if len(args) > 0 && args[0] == "incognito" {
		isIncognito = true
		prompt = "Browser (Incognito)"
	}
	selection, err := dmenuSelect(prompt, profiles)
	if err != nil {
		return err
	}
	return runBrowser(selection, isIncognito, []string{})
}

func doSnipit(args []string) error {
	// slurp | grim -g - - | blobby snipit.upload "$title" | wl-copy
	if len(args) < 1 {
		return fmt.Errorf("title not provided")
	}
	title := args[0]
	var width, height, x, y int
	cmd := exec.Command("slurp")
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stdin, cmd.Stderr = nil, os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	if _, err := fmt.Sscanf(string(out.Bytes()), "%d,%d %dx%d",
		&x, &y, &width, &height); err != nil {
		return err
	}
	cmd = exec.Command("grim", "-g",
		fmt.Sprintf("%d,%d %dx%d", x, y, width, height), "-")
	var img bytes.Buffer
	cmd.Stdin, cmd.Stdout, cmd.Stderr = nil, &img, os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	if err := os.WriteFile(os.ExpandEnv("$HOME/Downloads/a.png"),
		img.Bytes(), 0o0600); err != nil {
		return err
	}
	cmd = exec.Command("blobby", "snipit.upload", title)
	var res bytes.Buffer
	cmd.Stdin, cmd.Stdout, cmd.Stderr = &img, &res, os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	cmd = exec.Command("wl-copy")
	cmd.Stdin, cmd.Stdout, cmd.Stderr = &res, os.Stdout, os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}

type Themed interface {
	SetTheme(theme string) error
}

type AlacrittyTheme struct {
	ThemeTmpl *template.Template
}

func AlacrittyThemeNew() *AlacrittyTheme {
	return &AlacrittyTheme{
		ThemeTmpl: kAlacrittyConfigTmpl,
	}
}

func (a *AlacrittyTheme) SetTheme(theme string) error {
	cmd := exec.Command("alacritty", "--version")
	cmd.Stdin, cmd.Stderr = nil, os.Stderr
	out, err := cmd.Output()
	if err != nil {
		return err
	}
	m := kAlacrittyVersionRE.FindStringSubmatch(string(out))
	if len(m) != 2 {
		return fmt.Errorf("alacritty version string not expected")
	}
	v, err := strconv.ParseFloat(m[1], 64)
	if err != nil {
		return err
	}
	importCommand := "general.import"
	if v < 0.14 {
		importCommand = "import"
	}
	f := filepath.Join(kHome, ".config/alacritty/alacritty.toml")
	if err := os.MkdirAll(filepath.Dir(f), 0700); err != nil {
		return err
	}
	file, err := os.Create(f)
	if err != nil {
		return err
	}
	defer file.Close()
	a.ThemeTmpl.Execute(file, struct {
		Home   string
		Import string
		Theme  string
	}{
		Home:   kHome,
		Import: importCommand,
		Theme:  theme,
	})

	return nil
}

type GsettingsTheme struct{}

func (th *GsettingsTheme) SetTheme(theme string) error {
	gtkTheme := "Adwaita"
	switch theme {
	case "dark":
		gtkTheme = "Adwaita-dark"
	case "light":
		gtkTheme = "Adwaita"
	}
	cmd := exec.Command("gsettings", "set", "org.gnome.desktop.interface",
		"gtk-theme", gtkTheme)
	cmd.Stdin, cmd.Stdout, cmd.Stderr = nil, nil, os.Stderr
	return cmd.Run()
}

type X11Theme struct{}

func (th *X11Theme) SetTheme(theme string) error {
	cmd := exec.Command("xrdb", "-load", filepath.Join(kHome, ".Xresources"))
	cmd.Stdin, cmd.Stdout, cmd.Stderr = nil, nil, os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	cmd = exec.Command("xrdb", "-merge", filepath.Join(kHome,
		fmt.Sprintf("Pool/base/etc/Xresources-%s", theme)))
	cmd.Stdin, cmd.Stdout, cmd.Stderr = nil, nil, os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	cmd = exec.Command("xrdb", "-get", "root.bg")
	cmd.Stdin, cmd.Stderr = nil, os.Stderr
	rootBg, err := cmd.Output()
	if err != nil {
		return nil
	}
	if bg := strings.TrimSpace(string(rootBg)); bg != "" {
		cmd = exec.Command("xsetroot", "-solid", bg)
		cmd.Stdin, cmd.Stdout, cmd.Stderr = nil, nil, os.Stderr
		if err := cmd.Run(); err != nil {
			return err
		}
	}
	cmd = exec.Command("i3-msg", "-q", "reload")
	cmd.Stdin, cmd.Stdout, cmd.Stderr = nil, nil, os.Stderr
	if err = cmd.Run(); err != nil {
		return err
	}
	return nil
}

type SwayTheme struct{}

func (th *SwayTheme) SetTheme(theme string) error {
	sourcePath := filepath.Join(kHome, "Pool/base/i3/sway.themes")
	destPath := filepath.Join(kHome, ".config/sway/theme")
	sourceFile, err := os.Open(sourcePath)
	if err != nil {
		return fmt.Errorf("failed to open source file: %w", err)
	}
	defer sourceFile.Close()
	destFile, err := os.Create(destPath)
	if err != nil {
		return err
	}
	defer destFile.Close()
	scanner := bufio.NewScanner(sourceFile)
	writer := bufio.NewWriter(destFile)
	state := "before"
	for scanner.Scan() {
		line := scanner.Text()
		switch state {
		case "before":
			if strings.HasPrefix(line, fmt.Sprintf("theme '%s' {", theme)) {
				state = "within"
			}
		case "within":
			if strings.HasPrefix(line, "}") {
				state = "after"
			} else if !strings.HasPrefix(strings.TrimSpace(line), "#") &&
				strings.TrimSpace(line) != "" {
				if _, err := writer.WriteString(line + "\n"); err != nil {
					return err
				}
			}
		case "after":
			// Do nothing in the "after" state.
		}
	}
	if err := scanner.Err(); err != nil {
		return err
	}
	if err := writer.Flush(); err != nil {
		return err
	}
	cmd := exec.Command("swaymsg", "reload")
	cmd.Stdin, cmd.Stdout, cmd.Stderr = nil, nil, os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}

type RofiTheme struct{}

func (th *RofiTheme) SetTheme(theme string) error {
	srcPath := filepath.Join(kHome, "Pool/base/i3/rofi-config.rasi")
	dstPath := filepath.Join(kHome, ".config/rofi/config.rasi")
	inFile, err := os.Open(srcPath)
	if err != nil {
		return fmt.Errorf("failed to open source file %s: %w", srcPath, err)
	}
	defer inFile.Close()
	outFile, err := os.Create(dstPath)
	if err != nil {
		return fmt.Errorf("error opening output file %s: %w", dstPath, err)
	}
	defer outFile.Close()
	writer := bufio.NewWriter(outFile)
	scanner := bufio.NewScanner(inFile)
	for scanner.Scan() {
		line := scanner.Text()
		if line == "@theme \"light\"" {
			_, err = fmt.Fprintf(writer, "@theme \"%s\"\n", theme)
		} else {
			_, err = writer.WriteString(line + "\n")
		}
		if err != nil {
			return fmt.Errorf("error writing to output file: %w", err)
		}
	}
	if err = scanner.Err(); err != nil {
		return fmt.Errorf("error reading input file: %w", err)
	}
	if err = writer.Flush(); err != nil {
		return err
	}
	return nil
}

type EmacsTheme struct{}

func (th *EmacsTheme) SetTheme(theme string) error {
	emacsTheme := theme
	switch theme {
	case "dark":
		emacsTheme = "plain-dark"
	case "light":
		emacsTheme = "plain-light"
	}
	cmd := exec.Command("emacsclient", "--eval", fmt.Sprintf(
		"(progn (setq custom-enabled-themes nil) (load-theme '%s))",
		emacsTheme))
	cmd.Stdin, cmd.Stdout, cmd.Stderr = nil, nil, os.Stderr
	return cmd.Run()
}

type MacosTheme struct{}

func (th *MacosTheme) SetTheme(theme string) error {
	var cmd *exec.Cmd
	if theme == "dark" {
		cmd = exec.Command("osascript", "-e", "tell application \"System Events\" to tell appearance preferences to set dark mode to true")
	} else {
		cmd = exec.Command("osascript", "-e", "tell application \"System Events\" to tell appearance preferences to set dark mode to false")
	}
	cmd.Stdin, cmd.Stdout, cmd.Stderr = nil, nil, os.Stderr
	return cmd.Run()
}

func doTheme(args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("Theme not specified")
	}
	theme := args[0]
	themed := []Themed{
		&EmacsTheme{},
	}
	if kDE.IsMacos() {
		themed = append(themed,
			&MacosTheme{},
		)
	} else {
		themed = append(themed,
			AlacrittyThemeNew(),
			&GsettingsTheme{},
			&X11Theme{},
			&RofiTheme{},
			&SwayTheme{},
		)
	}
	for _, th := range themed {
		if err := th.SetTheme(theme); err != nil {
			return err
		}
	}
	return nil
}

func doREPL(args []string) error {
	greeting := ""
	p := "→"
	b := bufio.NewScanner(os.Stdin)
	for fmt.Printf("%s%s ", greeting, p); b.Scan(); fmt.Printf("%s ", p) {
		line := b.Text()
		a := strings.Split(line, " ")
		if len(a) == 0 {
			continue
		}
		verb := strings.TrimSpace(a[0])
		if err := Dispatch(verb, a[1:]); err != nil {
			fmt.Fprintf(os.Stderr, "Error: %s", err)
			continue
		}
		// fmt.Printf("\n# INPUT: %s\n", t)
	}
	return nil
}

func Dispatch(verb string, args []string) error {
	switch verb {
	case "ee", "emacsclient":
		cmd := exec.Command("emacsclient") // Fix args later.
		cmd.Env = os.Environ()
		topic, _ := focusedTopic()
		topicDir, _ := focusedTopicDir()
		if topic != "" {
			if _, err := os.Stat(topicDir); err == nil {
				cmd.Dir = topicDir
			}
			cmd.Env = append(cmd.Env,
				fmt.Sprintf("QUEST=%s", topic),
				fmt.Sprintf("QUESTDIR=%s", topicDir),
				fmt.Sprintf("QUESTSDIR=%s", topical.TopicsDir))
			expandFn := func(k string) string {
				if k == "QUEST" {
					return topic
				}
				if k == "QUESTDIR" {
					return topicDir
				}
				if k == "QUESTSDIR" {
					return topical.TopicsDir
				}
				return os.Getenv(k)
			}
			expandedArgs := make([]string, len(args))
			for i, arg := range args {
				expandedArgs[i] = os.Expand(arg, expandFn)
			}
			cmd.Args = append([]string{"emacsclient", "-a", "", "-nc"}, args...)
		}
		cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
		return cmd.Run()
	case "emacsclient-topicdir":
		topicDir, err := focusedTopicDir()
		if err != nil {
			return err
		}
		cmd := exec.Command("emacsclient", "-a", "''", "-nc", topicDir)
		cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
		return cmd.Run()
	case "emacsclient-tea":
		topic, err := focusedTopic()
		if err != nil {
			return err
		}
		cmd := exec.Command("emacsclient", "-a", "''", "-nc",
			path.Join(topical.TopicsDir, topic))
		cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
		return cmd.Run()
	case "term":
		return doTerm(args)
	case "term-screen":
		return doTermScreen(args)
	case "audio":
		return doAudio(args)
	case "brightness":
		return doBrightness(args)
	case "browse":
		return doBrowse(false, args)
	case "browse-incognito":
		return doBrowse(true, args)
	case "browser-pick":
		return doBrowserPick(args)
	case "snipit":
		return doSnipit(args)
	case "theme":
		return doTheme(args)
	case "all-topics":
		res, err := allTopics(args)
		if err != nil {
			return err
		}
		for _, x := range res {
			fmt.Println(x)
		}
		return nil
	case "switch-topic":
		topics := []string{}
		topics, err := potentialTopics(args)
		if err != nil {
			topics = []string{}
		}
		topic, err := dmenuSelect("Switch to topic", topics)
		if err != nil {
			return err
		}
		if topic == "" {
			return fmt.Errorf("Topic cannot be empty")
		}
		_, err = i3.RunCommand(fmt.Sprintf("workspace %s", topic))
		return err
	case "switch-topic-active":
		topics, err := allTopics(args)
		if err != nil {
			return err
		}
		topic, err := dmenuSelect("Switch to topic", topics)
		if err != nil {
			return err
		}
		if topic == "" {
			return fmt.Errorf("Topic cannot be empty")
		}
		_, err = i3.RunCommand(fmt.Sprintf("workspace %s", topic))
		return err
	case "move-topic":
		topics, err := allTopics(args)
		if err != nil {
			return err
		}
		topic, err := dmenuSelect("Move to topic", topics)
		if err != nil {
			return err
		}
		if topic == "" {
			return fmt.Errorf("Topic cannot be empty")
		}
		_, err = i3.RunCommand(fmt.Sprintf("move to workspace %s", topic))
		return err
	case "rename-topic":
		topic, err := focusedTopic()
		if err != nil {
			return err
		}
		newTopic, err := dmenuSelect(
			fmt.Sprintf("Rename topic %s to", topic), []string{topic})
		if err != nil {
			return err
		}
		if newTopic == "" {
			return fmt.Errorf("Topic cannot be empty")
		}
		_, err = i3.RunCommand(fmt.Sprintf("rename workspace to %s", newTopic))
		return err
	case "topics":
		res, err := potentialTopics(args)
		if err != nil {
			return err
		}
		for _, x := range res {
			fmt.Println(x)
		}
		return nil
	case "focused-topic":
		topic, err := focusedTopic()
		if err != nil {
			return err
		}
		topicInfo, err := topical.Get(topic)
		if err == nil {
			fmt.Println(topic, topicInfo)
		} else {
			fmt.Println(topic)
		}
		return nil
	case "marks":
		res, err := allMarks(args)
		if err != nil {
			return err
		}
		for _, x := range res {
			fmt.Println(x)
		}
		return nil
	case "i3status":
		ret, err := i3status(args)
		if err != nil {
			return err
		}
		fmt.Println(ret)
		return nil
	case "t":
		if len(args) < 1 {
			topicalscreen.ListSessions() // ignore errors.
			return nil
		}
		topic := args[0]
		screenAttachArgs := args[1:]
		return topicalscreen.Attach(topic, topical.TopicsDir, screenAttachArgs)
	case "listen":
		ret, err := listen(args)
		if err != nil {
			return err
		}
		fmt.Println(ret)
		return nil
	case "repl":
		return doREPL(args)
	default:
		fmt.Println(Usage())
		return nil
	}
	return errors.New(fmt.Sprintf("Verb '%s' not recognized", verb))
}

func Usage() string {
	return `
Usage:
  hub VERB ARGS...
` +
		// (let ((l nil))                                                                         (save-excursion                                                                        (save-restriction                                                                      (goto-char (point-min))                                                              (search-forward "func Dispatch")                                                     (narrow-to-defun)                                                                    (goto-char (point-min))                                                              (while (search-forward-regexp "^.*case " nil t)                                        (let* ((s (buffer-substring-no-properties (point)                                                                              (line-end-position)))                             (x (replace-regexp-in-string "\"\\(.*\\)\":" "\\1" s)))                         (push x l)))))                                                               (insert "\n`\n  Verbs:\n")                                                           (dolist (x (sort l #'string<))                                                         (insert (format "    %s\n" x)))                                                    (insert "`\n"))
		`
  Verbs:
    all-topics
    audio
    brightness
    browse
    browse-incognito
    browser-pick
    ee
    emacsclient
    emacsclient-tea
    emacsclient-topicdir
    focused-topic
    i3status
    listen
    marks
    move-topic
    rename-topic
    repl
    snipit
    switch-topic
    switch-topic-active
    t
    term
    term-screen
    theme
    topics
`
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal(Usage())
	}
	verb := os.Args[1]
	args := os.Args[2:]
	if err := Dispatch(verb, args); err != nil {
		log.Fatal(err)
	}
}

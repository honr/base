// This is equivalent to: | nc -NU $XDG_RUNTIME_DIR/pasteboard.client
package main

import (
	"io"
	"net"
	"os"
)

const sockpath = "$XDG_RUNTIME_DIR/pasteboard.client"

func main() {
	conn, err := net.Dial("unix", os.ExpandEnv(sockpath))
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	if _, err := io.Copy(conn, os.Stdin); err != nil {
		panic(err)
	}
}

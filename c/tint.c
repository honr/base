#include "tint.h"
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

typedef enum {
  TARGET_FG = 38,
  TARGET_BG = 48,
} Target;

int fprint_partial_color(FILE* output, const Target target, const char* color) {
  if (color[0] == '\0') {
    return 0;
  }
  int x = (int)strtol(color, NULL, 16);
  if (strlen(color) == 1 && color[0] == '-') {
    if (target == TARGET_FG) {
      fputs("39", output);
    } else if (target == TARGET_BG) {
      fputs("49", output);
    }
    return 0;
  }
  if (strlen(color) == 3) {
    fprintf(output, "%d;2;%d;%d;%d",
            target, 0x11 *(x >> 8 & 0xF), 0x11*(x >> 4 & 0xF), 0x11*(x & 0xF));
    return 0;
  }
  if (strlen(color) == 6) {
    fprintf(output, "%d;2;%d;%d;%d",
            target, x >> 16 & 0xFF, x >> 8 & 0xFF, x & 0xFF);
    return 0;
  }
  return -1;
}

int fprint_color_or_token_destructively(FILE* output, char* mut_token) {
  int len = strlen(mut_token);
  if (len < 2 || mut_token[0] != '#') {
    fputs(mut_token, output);
    fputc(' ', output);
    return 1;
  }
  mut_token++;
  char *fg = strsep(&mut_token, "/");
  char *bg = strsep(&mut_token, "/");
  if (mut_token != NULL) {
    // Error: trailing junk after colour codes, such as:  #fff/000/junk.
    return -1; // TODO: Bold, etc.
  }
  fputs("\x1b[", output);
  if (bg[0] == '\0' && fg[0] == '\0') {
    fputs("0m", output); // Special case: reset all.
    return 0;
  }
  if (fprint_partial_color(output, TARGET_FG, fg) < 0) {
    return -1;
  }
  if (fg[0] != '\0' && bg[0] != '\0') {
    fputs(";", output); // delimiter.
  }
  if (fprint_partial_color(output, TARGET_BG, bg) < 0) {
    return -1;
  }
  fputs("m", output);
  return 0;
}

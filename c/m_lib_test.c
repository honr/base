#include "m_lib.h"

#include <assert.h>
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

void test_slice_resize_null_slice() {
  m_slice* s = NULL;
  m_err err = m_slice_resize(s, 10, 10);
  assert(err == EINVAL);
}

void test_slice_resize_from_empty() {
  m_slice s = {
      .dat = NULL,
      .len = 0,
      .cap = 0,
  };
  m_err err = m_slice_resize(&s, 20, 20);
  assert(err == 0);
  assert(s.len == 20);
  assert(s.cap >= 20);  // Cap might be more due to doubling
  free(s.dat);
}

void test_slice_resize_no_realloc() {
  m_slice s = {
      .dat = malloc(20),
      .len = 5,
      .cap = 20,
  };
  m_err err = m_slice_resize(&s, 10, 10);
  assert(err == 0);
  assert(s.len == 10);
  assert(s.cap == 20);
  free(s.dat);
}

void test_slice_resize_realloc_needed() {
  m_slice s = {
      .dat = malloc(10),
      .len = 5,
      .cap = 10,
  };
  m_err err = m_slice_resize(&s, 20, 20);
  assert(err == 0);
  assert(s.len == 20);
  assert(s.cap >= 20);  // Cap might be more due to doubling
  free(s.dat);
}

void test_slice_resize_min_cap_adjust() {
  int64_t c = 8;
  m_slice s = {
      .dat = NULL,
      .len = c * 2,
      .cap = 0,
  };
  m_err err = m_slice_resize(&s, c * 2, c);
  assert(err == 0);
  assert(s.len == c * 2);
  assert(s.cap == c * 2);
  free(s.dat);

  s = (m_slice){
      .dat = NULL,
      .len = c * 2,
      .cap = 0,
  };
  err = m_slice_resize(&s, c * 2, 0);  // min_cap of 0
  assert(err == EINVAL);
  free(s.dat);

  s = (m_slice){
      .dat = NULL,
      s.len = 2 * c,
      s.cap = 0,
  };
  err = m_slice_resize(&s, 2 * c, -c);  // min_cap negative
  assert(err == EINVAL);
  free(s.dat);
}

int main() {
  test_slice_resize_null_slice();
  test_slice_resize_from_empty();
  test_slice_resize_no_realloc();
  test_slice_resize_realloc_needed();
  test_slice_resize_min_cap_adjust();
  printf("All tests passed!\n");
  return 0;
}

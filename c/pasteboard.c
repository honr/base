// cc -Os -o $HOME/.local/bin/pasteboard m_lib.c pasteboard.c -lX11

//pkg-config: x11

#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <err.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/un.h>
#include <unistd.h>

#include "m_lib.h"  // prefix: `m_`.

// The path is $XDG_RUNTIME_DIR/pasteboard.server
m_err set_sockpath(m_slice* path) {
  char* xdg_runtime_dir = getenv("XDG_RUNTIME_DIR");
  if (xdg_runtime_dir == NULL) {
    return m_error(EINVAL, "XDG_RUNTIME_DIR not set");
  }
  char* filename = "pasteboard.server";
  int len = strlen(xdg_runtime_dir) + 1 + strlen(filename) + 1;
  m_slice_resize(path, len, 8);
  path->len =
      snprintf(path->dat, path->cap, "%s/%s", xdg_runtime_dir, filename);
  return 0;
}

int bind_and_listen() {
  const int socket_backlog = 1;
  m_slice path = m_slice_make(0);
  m_err err = set_sockpath(&path);
  if (err != 0) {
    return -1;
  }
  unlink(path.dat);  // Ignore errors.
  int sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
  if (sockfd < 0) {
    perror("socket");
    return -1;
  }
  struct sockaddr_un addr;
  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  if (path.len >= sizeof(addr.sun_path)) {
    perror("sockpath too long");
    close(sockfd);
    return -1;
  }
  strncpy(addr.sun_path, path.dat, path.len);
  if (bind(sockfd, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
    perror("bind");
    close(sockfd);
    return -1;
  }
  if (listen(sockfd, socket_backlog) < 0) {
    perror("listen");
    close(sockfd);
    return -1;
  }
  fprintf(stderr, "Listening on socket %s\n", addr.sun_path);
  m_slice_close(&path);
  return sockfd;
}

// Assumes you already checked sockfd >= 0 and buffer != NULL.
m_err receive_all(int sockfd, m_slice* buf) {
  static const int64_t min_headroom = 4096;
  int client_sockfd = accept(sockfd, NULL, NULL);
  if (client_sockfd < 0) {
    perror("accept");
    return 1;
  }
  buf->len = 0;
  for (;;) {
    if ((buf->cap - buf->len) < min_headroom) {
      m_err err = m_slice_resize(buf, buf->cap * 2, min_headroom * 2);
      if (err != 0) {
        return m_error(err, "slice_resize");
      }
    }
    int bytes_read =
        recv(client_sockfd, buf->dat + buf->len, buf->cap - buf->len, 0);
    if (bytes_read < 0) {
      perror("recv");
      close(client_sockfd);
      return EIO;
    }
    buf->len += bytes_read;
    if (bytes_read == 0) {  // EOF
      break;
    }
  }
  close(client_sockfd);
  return 0;
}

int main() {
  Display* display = XOpenDisplay(NULL);
  if (display == NULL) {
    fprintf(stderr, "Error: Failed to open display.\n");
    return 1;
  }
  int x11fd = ConnectionNumber(display);
  Atom clipboard_atom = XInternAtom(display, "CLIPBOARD", False);
  Atom utf8stringAtom = XInternAtom(display, "UTF8_STRING", False);
  Atom stringAtom = XInternAtom(display, "STRING", False);
  Atom textAtom = XInternAtom(display, "TEXT", False);
  Atom textPlainAtom = XInternAtom(display, "text/plain", False);
  Atom targetsAtom = XInternAtom(display, "TARGETS", False);
  Atom timestampAtom = XInternAtom(display, "TIMESTAMP", False);
  Window win = DefaultRootWindow(display);
  m_slice buf = {
      .dat = NULL,  // First usage will allocate memory for it.
      .len = 0,
      .cap = 0,
  };
  int sockfd = bind_and_listen();
  if (sockfd < 0) {
    fprintf(stderr, "Error: Failed to get sockfd.\n");
    return 1;
  }
  int64_t selectionLastUpdateTime = m_current_time_msec();
  for (;;) {
    fd_set read_fds;
    FD_ZERO(&read_fds);
    FD_SET(x11fd, &read_fds);
    FD_SET(sockfd, &read_fds);
    int max_fd = (x11fd > sockfd ? x11fd : sockfd) + 1;
    if (select(max_fd, &read_fds, NULL, NULL, NULL) == -1) {
      perror("select");
      return -1;
    }
    if (FD_ISSET(sockfd, &read_fds)) {
      int r = receive_all(sockfd, &buf);
      selectionLastUpdateTime = m_current_time_msec();
      XSetSelectionOwner(display, clipboard_atom, win, CurrentTime);
      if (XGetSelectionOwner(display, clipboard_atom) != win) {
        fprintf(stderr, "Error: Could not acquire clipboard ownership\n");
        continue;  // Or handle error as needed
      }
    }
    if (FD_ISSET(x11fd, &read_fds)) {
      XEvent event;
      XNextEvent(display, &event);
      if (event.type == SelectionRequest &&
          event.xselectionrequest.selection == clipboard_atom) {
        XSelectionRequestEvent* req = &event.xselectionrequest;
        if (req->target == utf8stringAtom || req->target == stringAtom ||
            req->target == textAtom || req->target == textPlainAtom) {
          XChangeProperty(display, req->requestor, req->property,
                          req->target, 8, PropModeReplace,
                          (unsigned char*)buf.dat, buf.len);
          XEvent reply = {
            .xselection =
            {
              .type = SelectionNotify,
              .display = display,
              .property = req->property,
              .requestor = req->requestor,
              .selection = clipboard_atom,
              .target = req->target,
              .time = req->time,
            },
          };
          Status status = XSendEvent(display, req->requestor, False, 0, &reply);
          XFlush(display);
        } else if (req->target == timestampAtom) {
          fprintf(stderr, "Recived TIMESTAMP event.\n");
          // Time time = (Time)selectionLastUpdateTime;
          // XChangeProperty(display, req->requestor, req->property, timestampAtom,
          //                 32, PropModeReplace, (unsigned char*)&time, 1);
        } else if (req->target == targetsAtom) {
          // TODO: Implement.
          XEvent reply = {
            .xselection =
            {
              .type = SelectionNotify,
              .display = display,
              .property = req->property,
              .requestor = req->requestor,
              .selection = clipboard_atom,
              .target = req->target,
              .time = req->time,
            },
          };
          Status status = XSendEvent(display, req->requestor, False, 0, &reply);
          XFlush(display);
          continue;
        } else {
          fprintf(stderr, "Received target (type) request %ld (%s)\n",
                  req->target, XGetAtomName(display, req->target));
          // XSendEvent(display, req->requestor, False, 0, NULL);
          // XFlush(display);
          continue;
        }
      }
    }
  }
  XCloseDisplay(display);
  m_slice_close(&buf);
  close(sockfd);
  return 0;
}

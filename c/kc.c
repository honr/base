//deps: m_lib libpkgconf

// $ cc -Wall -Og -o $HOME/.local/bin/kc kc.c $(pkgconf --cflags --libs libpkgconf m_lib)

#include <err.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/wait.h>
#include <libpkgconf/libpkgconf.h>

#include "m_lib.h"  // prefix: `m_`.

m_err process_deps(char* line, pkgconf_client_t* pc, m_slice* requires) {
  // m_slice buf = *requires;
  /* int64_t cur = 0; */
  m_err err;
  int64_t count = 0;
  char* saveptr;
  char* token;
  char** tokens = NULL;
  token = strtok_r(line, " \t\n", &saveptr);
  while (token != NULL) {
    if (token[0] == '"') {
      char quote = token[0];
      char* end_quote = strchr(token + 1, quote);
      if (end_quote == NULL) {
        fprintf(stderr, "Invalid pre-preamble.\n");
        return EINVAL;
      }
      *end_quote = '\0';
      token = token + 1;
    }
    tokens = realloc(tokens, (count + 1)*sizeof(char*));
    if (tokens == NULL) {
      return ENOMEM;
    }
    tokens[count] = strdup(token);
    if (tokens[count] == NULL) {
      return ENOMEM;
    }
    count++;
    token = strtok_r(NULL, " \t\n", &saveptr);
  }
  // "$(pkgconf --cflags --libs "
  int64_t cur = requires->len-1;
  for (int64_t i = 0; i < count; ++i) {
    pkgconf_pkg_t* pkg = pkgconf_pkg_find(pc, tokens[i]);
    if (pkg == NULL) {
      perror("finding pkg");
      return EINVAL;
    }
    err = m_slice_write_str(requires, &cur, tokens[i]);
    if (err != 0) {
      return err;
    }
    err = m_slice_write_str(requires, &cur, " ");
    if (err != 0) {
      return err;
    }
  }
  // WIP
  return 0;
}

m_err process_file(const char *filename, pkgconf_client_t* pc) {
  static const char prefix[] = "//deps:";
  FILE* file = fopen(filename, "r");
  if (file == NULL) {
    return errno;
  }
  // TODO: Don't require deps to be all on the same line.
  m_slice requires = m_slice_empty_str(1024);
  m_slice buf = m_slice_empty_str(1024);
  while (true) {
    // Include the null terminator in buf.len.
    buf.len = 1 + getline(&(buf.dat), (size_t*)&(buf.cap), file);
    if (buf.len <= 0) {
      break;
    }
    buf.dat[buf.len - 1] = '\0'; // Ensure null termination.
    char* line = buf.dat;
    if (strncmp(line, prefix, sizeof(prefix) - 1) == 0) {
      m_err err = process_deps(line + sizeof(prefix) - 1, pc, &requires);
      if (err != 0) {
        return err;
      }
    } else if (line[0] != '/' && line[0] != '\n') {
      break;
    }
  }
  fclose(file);
  printf("~~ requires: '%s'\n", requires.dat);
  return 0;
}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Usage: %s <file1> <file2> ...\n", argv[0]);
    return 1;
  }
  pkgconf_cross_personality_t* personality =
      pkgconf_cross_personality_default();
  pkgconf_client_t* pc = pkgconf_client_new(NULL, NULL, personality);
  if (pc == NULL) {
    fprintf(stderr, "pkgconf client init failed.\n");
    return 1;
  }
  m_err err;
  for (int i = 1; i < argc; ++i) {
    err = process_file(argv[i], pc);
    if (err != 0) {
      return err;
    }
  }
  pkgconf_client_deinit(pc);
  pkgconf_cross_personality_deinit(personality);
  return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

// If there is one argument, the SUFFIX, sockpath will be
// $XDG_RUNTIME_DIR/pasteboard.SUFFIX
int main(int argc, char** argv) {
  char* suffix = "client";
  if (argc > 1) {
    suffix = argv[1];
  }
  int sockfd;
  struct sockaddr_un addr;
  char sockpath[sizeof(addr.sun_path)];
  const char *env_path = getenv("XDG_RUNTIME_DIR");
  if (env_path == NULL) {
    fprintf(stderr, "Error: env var XDG_RUNTIME_DIR not set.\n");
    return 1;
  }
  snprintf(sockpath, sizeof(sockpath), "%s/pasteboard.%s", env_path, suffix);
  if ((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
    perror("socket error");
    return 1;
  }
  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, sockpath, sizeof(addr.sun_path) - 1);
  if (connect(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    perror("connect error");
    close(sockfd);
    return 1;
  }
  // cat /proc/sys/net/core/wmem_default -> 212992 (SO_SNDBUF=13*16384).
  char buffer[16384];
  ssize_t bytes_read;
  while ((bytes_read = read(STDIN_FILENO, buffer, sizeof(buffer))) > 0) {
    if (write(sockfd, buffer, bytes_read) < 0) {
      perror("write error");
      close(sockfd);
      return 1;
    }
  }
  close(sockfd);
  if (bytes_read < 0) {
    perror("read error");
    return 1;
  }
  return 0;
}

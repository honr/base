// C includes:
#ifdef __cplusplus
extern "C" {
#endif
#include "tint.h"
#ifdef __cplusplus
}
#endif

#include <gtest/gtest.h>

// Tests for is_hex_color function
TEST(IsHexColorTest, Valid3Digit) {
  char fg_color[8];
  char bg_color[8];
  EXPECT_TRUE(is_hex_color("#123", fg_color, bg_color));
  EXPECT_STREQ(fg_color, "123");
  EXPECT_STREQ(bg_color, "");
}

TEST(IsHexColorTest, Valid6Digit) {
  char fg_color[8];
  char bg_color[8];
  EXPECT_TRUE(is_hex_color("#123456", fg_color, bg_color));
  EXPECT_STREQ(fg_color, "123456");
  EXPECT_STREQ(bg_color, "");
}

TEST(IsHexColorTest, Valid3DigitWithBackground) {
  char fg_color[8];
  char bg_color[8];
  EXPECT_TRUE(is_hex_color("#123/456", fg_color, bg_color));
  EXPECT_STREQ(fg_color, "123");
  EXPECT_STREQ(bg_color, "456");
}

TEST(IsHexColorTest, Valid6DigitWithBackground) {
  char fg_color[8];
  char bg_color[8];
  EXPECT_TRUE(is_hex_color("#123456/789ABC", fg_color, bg_color));
  EXPECT_STREQ(fg_color, "123456");
  EXPECT_STREQ(bg_color, "789ABC");
}

TEST(IsHexColorTest, InvalidShort) {
  char fg_color[8];
  char bg_color[8];
  EXPECT_FALSE(is_hex_color("#12", fg_color, bg_color));
}

TEST(IsHexColorTest, InvalidNoHash) {
  char fg_color[8];
  char bg_color[8];
  EXPECT_FALSE(is_hex_color("123456", fg_color, bg_color));
}

TEST(IsHexColorTest, InvalidCharacters) {
  char fg_color[8];
  char bg_color[8];
  EXPECT_FALSE(is_hex_color("#12G456", fg_color, bg_color));
}

TEST(IsHexColorTest, InvalidSlashPosition1) {
  char fg_color[8];
  char bg_color[8];
  EXPECT_FALSE(is_hex_color("#1/23", fg_color, bg_color));
}

TEST(IsHexColorTest, InvalidSlashPosition2) {
  char fg_color[8];
  char bg_color[8];
  EXPECT_FALSE(is_hex_color("#123/", fg_color, bg_color));
}

TEST(IsHexColorTest, InvalidSlashPosition3) {
  char fg_color[8];
  char bg_color[8];
  EXPECT_FALSE(is_hex_color("#/123", fg_color, bg_color));
}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

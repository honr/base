#include "tint.h"
#include <stdio.h>
#include <string.h>

// Helper function to compare strings
int str_equal(const char* str1, const char* str2) {
  return strcmp(str1, str2) == 0;
}

int main() {
  char fg_color[8];
  char bg_color[8];
  int test_passed = 1;

  // Test case 1: Valid 3-digit hex color
  if (!is_hex_color("#123", fg_color, bg_color) || !str_equal(fg_color, "123") ||
      bg_color[0] != '\0') {
    printf("Test case 1 failed: #123\n");
    test_passed = 0;
  }

  // Test case 2: Valid 6-digit hex color
  if (!is_hex_color("#123456", fg_color, bg_color) ||
      !str_equal(fg_color, "123456") || bg_color[0] != '\0') {
    printf("Test case 2 failed: #123456\n");
    test_passed = 0;
  }

  // Test case 3: Valid 3-digit hex color with background
  if (!is_hex_color("#123/456", fg_color, bg_color) ||
      !str_equal(fg_color, "123") || !str_equal(bg_color, "456")) {
    printf("Test case 3 failed: #123/456\n");
    test_passed = 0;
  }

  // Test case 4: Valid 6-digit hex color with background
  if (!is_hex_color("#123456/789ABC", fg_color, bg_color) ||
      !str_equal(fg_color, "123456") || !str_equal(bg_color, "789ABC")) {
    printf("Test case 4 failed: #123456/789ABC\n");
    test_passed = 0;
  }

  // Test case 5: Invalid hex color (too short)
  if (is_hex_color("#12", fg_color, bg_color)) {
    printf("Test case 5 failed: #12\n");
    test_passed = 0;
  }

  // Test case 6: Invalid hex color (no #)
  if (is_hex_color("123456", fg_color, bg_color)) {
    printf("Test case 6 failed: 123456\n");
    test_passed = 0;
  }

  // Test case 7: Invalid hex color (invalid characters)
  if (is_hex_color("#12G456", fg_color, bg_color)) {
    printf("Test case 7 failed: #12G456\n");
    test_passed = 0;
  }

  // Test case 8: Invalid hex color (invalid slash position)
  if (is_hex_color("#1/23", fg_color, bg_color)) {
    printf("Test case 8 failed: #1/23\n");
    test_passed = 0;
  }

  // Test case 9: Invalid hex color (invalid slash position)
  if (is_hex_color("#123/", fg_color, bg_color)) {
    printf("Test case 9 failed: #123/\n");
    test_passed = 0;
  }

  // Test case 10: Invalid hex color (invalid slash position)
  if (is_hex_color("#/123", fg_color, bg_color)) {
    printf("Test case 10 failed: #/123\n");
    test_passed = 0;
  }

  if (test_passed) {
    printf("All test cases passed!\n");
  } else {
    printf("Some test cases failed.\n");
  }

  return 0;
}

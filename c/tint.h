#pragma once
#include <stdio.h>

// Prints the token to the specified output.  The token can get mutated as part
// of the process.  Use strdup before calling if needed.  Returns number of
// non-colour tokens printed out, or -1 in case of an error.
int fprint_color_or_token_destructively(FILE* output, char* mut_token);

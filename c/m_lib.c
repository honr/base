// $ cc -Os -Wall -shared -fPIC m_lib.c -o $HOME/.local/lib/libm_lib.so

#include "m_lib.h"

#include <err.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

static const int64_t MIN_CAP = 64;

void m_slice_clear(m_slice s) {
  if (s.len > 0) {
    s.len = 0;
  }
  return;
}

// TODO: return status and value, not just value.
m_slice m_slice_make(int64_t cap) {
  char* dat = malloc(cap);
  return (m_slice){.dat = dat, .len = 0, .cap = cap};
}

m_slice m_slice_empty_str(int64_t cap) {
  if (cap < 1) {
    cap = MIN_CAP;
  }
  char* dat = malloc(cap);
  dat[0] = 0;
  return (m_slice){.dat = dat, .len = 1, .cap = cap};
}

m_err m_slice_resize(m_slice* s, int64_t cap, int64_t min_cap) {
  if (s == NULL) {
    return EINVAL;
  }
  if (min_cap < 4) {
    return EINVAL;
  }
  if (s->dat != NULL && cap <= s->cap) {
    return 0;
  }
  s->cap = min_cap;
  while (s->cap < cap) {
    s->cap <<= 1;
  }
  s->dat = realloc(s->dat, s->cap);
  if (s->dat == NULL) {
    // perror("malloc");
    return ENOMEM;
  }
  return 0;
}

m_err m_slice_close(m_slice* s) {
  if (s == NULL) {
    return EINVAL;
  }
  if (s->dat != NULL) {
    free(s->dat);
  }
  return 0;
}

// Assumes the slice is null terminated, and cur points at the termination
// character.  In other words, "" is {.cap: &[0], len: 1, cap: 1}, cur=0.
m_err m_slice_write_str(m_slice* s, int64_t* cur, const char* str) {
  int64_t n = strlen(str);
  int64_t len = *cur + n + 1;
  m_err err = m_slice_resize(s, len, MIN_CAP);
  if (err != 0) {
    return err;
  }
  memcpy(s->dat + *cur, str, n);
  *cur += n;
  *(s->dat + *cur) = '\0';
  s->len = len;
  return 0;
}

int64_t m_current_time_msec() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return (int64_t)tv.tv_sec * 1000LL + (int64_t)tv.tv_usec / 1000LL;
}

m_err m_error(m_err err, char* msg) {
  errno = err;
  perror(msg);
  return err;
}

#include "tint.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

typedef enum {
  MODE_NORMAL,
  MODE_ERROR
} Mode;

int main(int argc, char* argv[]) {
  Mode mode_enum;
  if (argc == 1) {
    mode_enum = MODE_NORMAL;
  } else if (argc == 2) {
    if (strcmp(argv[1], "err") == 0) {
      mode_enum = MODE_ERROR;
    } else {
      fprintf(stderr, "Usage:\n$ %s\n$ %s err\n", argv[0], argv[0]);
      return 1;
    }
  } else {
    fprintf(stderr, "Usage:\n$ %s\n$ %s err\n", argv[0], argv[0]);
    return 1;
  }

  char token[256];
  FILE* output = stdout;
  if (mode_enum == MODE_ERROR) {
    fprintf(stderr, "%s\n", strerror(errno));
    output = stderr;
  }
  while (scanf("%255s", token) == 1) {
    if (fprint_color_or_token_destructively(output, token) < 0) {
      return EINVAL;
    }
  }
  fprintf(output, "\x1b[0m\n");
  return 0;
}

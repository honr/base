(dolist (dir '("~/.emacs.d/site-lisp"
               "/usr/share/emacs/site-lisp"))
  (when (file-exists-p dir)
    (let ((default-directory dir))
      (normal-top-level-add-to-load-path (list default-directory))
      (normal-top-level-add-subdirs-to-load-path))))
;; (setq Info-additional-directory-list
;;       (mapcar 'expand-file-name
;; 	      ;; The trailing slash is *IMPORTANT*.
;;               (list "~/.nix-profile/share/info/")))
(setq custom-theme-load-path '("~/.emacs.d/themes" t))
(setq custom-safe-themes t)
(when (boundp 'tool-bar-mode) (tool-bar-mode -1))

;; OS-specific configurations:
(cond
 ((eq system-type 'darwin)
  (require 'mac-frame)
  (setq texmf-root (expand-file-name "~/Library/texmf"))
  (setq dired-use-ls-dired nil)
  (setq ispell-program-name "/opt/local/bin/aspell")
  (when (or (eq window-system 'ns) (eq window-system 'nextstep))
    (set-face-attribute 'default nil :family "Spline Sans Mono" :height 140)
    (add-to-list 'default-frame-alist '(alpha 95 95))
    (if (equal "dark" (ns-do-applescript
                       (concat "tell application \"System Events\" to "
                               "tell appearance preferences\n"
                               "  if get dark mode then\n"
                               "    \"dark\"\n"
                               "  else\n"
                               "    \"light\"\n"
                               "  end if\n"
                               "end tell")))
        (load-theme 'plain-dark)
      (load-theme 'plain-light))
    (server-start)))

 ((eq system-type 'gnu/linux)
  (setq x-select-enable-clipboard t)
  (menu-bar-mode -1)
  (add-to-list 'default-frame-alist '(scroll-bar-width . 11))
  (with-eval-after-load 'browse-url
    (defun browse-url-default-browser (url &rest ignored-args)
      (interactive (browse-url-interactive-arg "URL: "))
      (setq url (browse-url-encode-url url))
      (let* ((process-environment (browse-url-process-environment)))
        (start-process "hub browse" nil "hub" "browse" url))))
  ;; ;; Redefine this funcion.
  ;; (defun browse-url-can-use-xdg-open () t)
  (cond
   ((featurep 'gtk)
    (set-face-attribute 'default nil
                        :family "Spline Sans Mono" :height 80))
   (t
    (set-face-attribute 'default nil
                        :family "Spline Sans Mono" :height 100)))))

(setq inhibit-startup-message t)
(setq backup-inhibited t)
(setq create-lockfiles nil)
(setq dired-dwim-target t)
(setq dired-recursive-copies 'always)
(setq dired-recursive-deletes 'always)
(show-paren-mode t)
(when (boundp 'scroll-bar-mode)
  (set-scroll-bar-mode 'right)
  (scroll-bar-mode -1))
(setq scroll-preserve-screen-position t)
(set-language-environment "UTF-8")
;; (setq completion-styles '(partial-completion initials))
;; (partial-completion-mode t)

;; Wrap the rest of the file in a condition such as the following, if a light
;; version of emacs is sometimes desired.
;; (when (or (daemonp) (and (functionp 'server-running-p) (server-running-p)))
;;   ...)

;; Add a "layer" of protection around ‘C-x C-c’.
(global-unset-key (kbd "C-x C-c"))
(global-set-key (kbd "C-x C-c C-x C-x C-c") 'save-buffers-kill-emacs)
(global-unset-key (kbd "C-z")) ;; C-z is not particularly useful either.
(setq savehist-file "~/.history/emacs")
(savehist-mode t)
(setq history-length 32768)
(setq disabled-command-function nil)
(line-number-mode t)
(column-number-mode t)
(blink-cursor-mode t)
(setq ring-bell-function 'ignore)
;; (mouse-avoidance-mode 'banish)
(setq mail-user-agent 'message-user-agent)
(setq default-directory "~/")
(setq-default abbrev-mode nil)
;; The original format is:
;;   (multiple-frames "%b" ("" invocation-name "@" system-name))
(setq frame-title-format (list "%b (%m)  +Emacs")) ; %b: buffer name, %m: mode.

(require 'dired)
(require 'dired-x)
(require 'wdired)
(require 'comint)
(require 'calendar)
(require 'term nil t)
(require 'tramp)
(require 'ibuffer nil t)
(require 'browse-url)
(when (require 'subword)
  (global-subword-mode t))
(require 'newcomment)
(require 'org)
;; python.el defines this as defvar-local instead of a defcustom, so we have to
;; circumvent it here.  Most calls in python.el are blocking at the moment, so
;; we should avoid them.
(defvar python-eldoc-get-doc nil)
(require 'vterm nil t)

;; (require 'subr-x)
;; (require 'map)
;; (require 'seq)
;; (require 'ewoc)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-source-save-behavior nil)
 '(calendar-date-display-form calendar-iso-date-display-form)
 '(compilation-skip-threshold 2)
 '(css-indent-offset 2)
 '(diary-date-forms diary-iso-date-forms)
 '(dired-listing-switches "-alo")
 '(ediff-window-setup-function 'ediff-setup-windows-plain)
 '(erc-button-mode nil)
 '(erc-default-server "irc.libera.chat" t)
 '(erc-echo-timestamp-format "Timestamped %a %Y-%m-%d %T %z w%V")
 '(erc-echo-timestamps t)
 '(erc-fill-mode nil)
 '(erc-insert-timestamp-function 'erc-insert-timestamp-left)
 '(erc-prompt ">")
 '(erc-server-reconnect-attempts 40)
 '(erc-server-reconnect-timeout 30)
 '(erc-timestamp-format "%R ")
 '(fill-column 80)
 '(flyspell-use-meta-tab nil)
 '(gud-gdb-command-name "gdb --annotate=1")
 '(indent-tabs-mode nil)
 '(js-indent-level 2)
 '(large-file-warning-threshold nil)
 '(major-mode 'org-mode)
 '(mouse-wheel-progressive-speed nil)
 '(mouse-wheel-scroll-amount '(3 ((shift) . 1) ((control))))
 '(next-screen-context-lines 8)
 '(org-agenda-files '("~/Arc/tea/agenda"))
 '(org-default-notes-file "~/Arc/tea/notes")
 '(org-export-with-broken-links t)
 '(org-export-with-special-strings nil)
 '(org-export-with-sub-superscripts nil)
 '(org-journal-date-format "%F w%W/%A")
 '(org-journal-dir "~/Arc/tea/journal")
 '(org-replace-disputed-keys t)
 '(org-startup-folded 'showall)
 '(org-todo-keywords '("TODO" "CANCELLED" "PROGRESSING" "DONE"))
 '(org-use-sub-superscripts nil)
 '(package-selected-packages
   '(auctex clojure-mode code-cells dart-mode geiser-guile go-mode llm magit
            markdown-mode nix-mode org-journal paredit rust-mode slime
            swift-mode typescript-mode))
 '(python-eldoc-get-doc nil t)
 '(python-guess-indent nil)
 '(python-indent-guess-indent-offset nil)
 '(python-indent-offset 2)
 '(read-buffer-completion-ignore-case t)
 '(read-file-name-completion-ignore-case t)
 '(safe-local-variable-directories '("/home/honar/.opt/src/emacs/"))
 '(safe-local-variable-values
   '((vc-default-patch-addressee . "bug-gnu-emacs@gnu.org")
     (etags-regen-ignores "test/manual/etags/")
     (etags-regen-regexp-alist
      (("c" "objc") "/[ \11]*DEFVAR_[A-Z_ \11(]+\"\\([^\"]+\\)\"/\\1/"
       "/[ \11]*DEFVAR_[A-Z_ \11(]+\"[^\"]+\",[ \11]\\([A-Za-z0-9_]+\\)/\\1/"))
     (indent-tabs-mode t)))
 '(sh-basic-offset 2)
 '(standard-indent 2)
 '(tab-width 2)
 '(vc-follow-symlinks t)
 '(vc-handled-backends nil)
 '(view-read-only t))

;; I haven't decided between visual-line-mode or truncate-lines.
;; (add-hook 'text-mode-hook #'visual-line-mode)
(add-hook 'text-mode-hook
          (lambda () (setq truncate-lines nil)))
(add-hook 'text-mode-hook #'flyspell-mode)

(setq ibuffer-formats '((mark modified read-only " " (name 40 -1) " "
                              (size 6 -1 :right) " " (mode 16 16 :center) " "
                              filename)
                        (mark " " (name 16 -1) " " filename)))

(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward
      uniquify-strip-common-suffix t
      uniquify-separator " *")

(with-eval-after-load 'flyspell
  (add-hook 'prog-mode-hook (lambda () (flyspell-prog-mode))))

;; TODO: find a better way to let tramp restart a connection.
(with-eval-after-load 'tramp
  (defun tramp-try-to-start-possibly-stall-remote (remote-path
                                                   buffer-name-of-tramp)
    (let ((buffer-of-tramp (get-buffer buffer-name-of-tramp)))
      (when (bufferp buffer-of-tramp)
        (kill-buffer buffer-of-tramp)))
    (sit-for .5)
    (find-file-noselect remote-path))
  ;; Let ssh decide on its own.
  (setq tramp-ssh-controlmaster-options ""))

(defun go-easy-on-remote ()
  (interactive)
  (remove-hook 'find-file-hooks 'vc-find-file-hook)
  (remove-hook 'find-file-hooks 'vc-refresh-state)
  (setq vc-handled-backends nil)
  (setq auto-save-default nil)
  (setq auto-save-timeout nil)
  (setq auto-save-interval 0))
(go-easy-on-remote)

(require 'cc-mode)
(require 'paredit)
(require 'lib-sexp)
(require 'clojure-mode nil t)
(require 'go-mode nil t)
(require 'nix-mode nil t)
(require 'protobuf-mode nil t)
(require 'dart-mode nil t)
(require 'javascript-mode nil t)
(require 'rust-mode nil t)
(require 'markdown-mode nil t)
(require 'python-mode nil t)

(add-hook 'prog-mode-hook
          (lambda () (setq show-trailing-whitespace t)))

(defface paren-face
  '((((class color) (background dark)) (:foreground "#648864"))
    (((class color) (background light)) (:foreground "grey60")))
  "Face used to dim parentheses.")

(dolist (mode (list 'lisp-interaction-mode 'emacs-lisp-mode 'lisp-mode))
  (font-lock-add-keywords mode '(("(\\|)" . 'paren-face)) 'append))

(dolist (hook (list 'emacs-lisp-mode-hook 'lisp-mode-hook))
  (add-hook hook (lambda () (paredit-mode +1))))


(with-eval-after-load 'clojure-mode
  (add-hook 'clojure-mode-hook
            (lambda ()
              (paredit-mode +1)
              (subword-mode t)))
  (font-lock-add-keywords
   'clojure-mode
   '(("(\\|)\\|\\[\\|\\]\\|{\\|}" . 'paren-face)) 'append)
  (add-to-list 'auto-mode-alist '("\\.clj$" . clojure-mode))
  (add-to-list 'auto-mode-alist '("/AA$" . clojure-mode)))

(setq nxml-sexp-element-flag t)

;; (setq c-electric-flag nil)
(electric-indent-mode -1)

(c-add-style "java"
             '((c-basic-offset . 2)
               (c-comment-only-line-offset 0 . 0)
               (c-offsets-alist
                (access-label . 0)
                (arglist-close . c-lineup-arglist)
                (arglist-intro . 4)
                (func-decl-cont . c-lineup-java-throws)
                (inher-cont . c-lineup-java-inher)
                (inline-open . 0)
                (knr-argdecl-intro . 5)
                (label . +)
                (statement-block-intro . +)
                (statement-case-open . +)
                (statement-cont c-lineup-assignments ++)
                (substatement-label . +)
                (substatement-open . +)
                (topmost-intro-cont . +))))

(defun c-style-template-args-cont (_)
  (save-excursion
    (beginning-of-line)
    (if (re-search-forward "^[\t ]*>" (line-end-position) t)
        0)))

;; Google C/C++ Programming Style.
;; From github.com/google/styleguide/google-c-style.el.  Further modified.
(c-add-style "google"
             `((c-recognize-knr-p . nil)
               (c-basic-offset . 2)
               (indent-tabs-mode . nil)
               (c-comment-only-line-offset . 0)
               (c-hanging-braces-alist . ((defun-open after)
                                          (defun-close before after)
                                          (class-open after)
                                          (class-close before after)
                                          (inexpr-class-open after)
                                          (inexpr-class-close before)
                                          (namespace-open after)
                                          (inline-open after)
                                          (inline-close before after)
                                          (block-open after)
                                          (block-close . c-snug-do-while)
                                          (extern-lang-open after)
                                          (extern-lang-close after)
                                          (statement-case-open after)
                                          (substatement-open after)))
               (c-hanging-colons-alist . ((case-label)
                                          (label after)
                                          (access-label after)
                                          (member-init-intro before)
                                          (inher-intro)))
               (c-hanging-semi&comma-criteria
                . (c-semi&comma-no-newlines-for-oneline-inliners
                   c-semi&comma-inside-parenlist
                   c-semi&comma-no-newlines-before-nonblanks))
               (c-indent-comments-syntactically-p . t)
               (comment-column . 40)
               (c-indent-comment-alist . ((other . (space . 2))))
               (c-cleanup-list . (brace-else-brace
                                  brace-elseif-brace
                                  brace-catch-brace
                                  empty-defun-braces
                                  defun-close-semi
                                  list-close-comma
                                  scope-operator))
               (c-offsets-alist . ((arglist-intro . 4)
                                   (func-decl-cont . ++)
                                   (member-init-intro . ++)
                                   (inher-intro . ++)
                                   (comment-intro . 0)
                                   (arglist-close . c-lineup-arglist)
                                   (topmost-intro . 0)
                                   (block-open . 0)
                                   (inline-open . 0)
                                   (substatement-open . 0)
                                   (statement-cont c-lineup-assignments ++)
                                   (label . /)
                                   (case-label . +)
                                   (statement-case-open . +)
                                   (statement-case-intro . +) ; case w/o {
                                   (access-label . /)
                                   (innamespace . 0)
                                   (inextern-lang . 0)
                                   (template-args-cont
                                    c-lineup-template-args +)))))

(setq c-default-style
      '((java-mode . "java")
        (awk-mode . "awk")
        (other . "google")))

;; (defun config-c-mode-hook () (glasses-mode t) (subword-mode t))
;; (add-hook 'c-mode-common-hook 'config-c-mode-hook)
;; (add-hook 'c-mode-common-hook (lambda () (flyspell-prog-mode)))

(with-eval-after-load 'go-mode
  (add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode)))

(with-eval-after-load 'nix-mode
  (add-to-list 'auto-mode-alist '("\\.nix\\'" . nix-mode)))

;; Has require 'cl, which is obsolete.
;; (when (require 'haskell-mode nil t)
;;   (require 'haskell-indentation)
;;   (require 'haskell-decl-scan)
;;   (require 'haskell-doc)
;;   (require 'inf-haskell)
;;   (require 'haskell-ghci nil t)
;;   (setq haskell-mode-hook '(turn-on-haskell-indentation
;;                             turn-on-eldoc-mode
;;                             turn-on-haskell-doc-mode
;;                             turn-on-haskell-decl-scan))
;;   (add-to-list 'auto-mode-alist '("\\.hs\\'" . haskell-mode)))

(with-eval-after-load 'protobuf-mode
  (add-to-list 'auto-mode-alist '("\\.proto\\'" . protobuf-mode)))

(with-eval-after-load 'python-mode
  (add-to-list 'auto-mode-alist '("/BUILD$" . python-mode))
  (add-to-list 'auto-mode-alist '("\\.bzl$" . python-mode)))

(with-eval-after-load 'dart-mode
  (add-to-list 'auto-mode-alist '("\\.dart\\'" . dart-mode)))

(with-eval-after-load 'javascript-mode
  (add-to-list 'auto-mode-alist '("\\.javascript\\'" . javascript-mode)))

(with-eval-after-load 'rust-mode
  (add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode)))

(with-eval-after-load 'markdown-mode
  (add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode)))

(defalias 'perl-mode 'cperl-mode)

(define-key c++-mode-map (kbd "; a n")
  (lambda ()
    (interactive)
    (insert (format "{\n\n}")) (backward-char 2) (c-indent-line)))

(define-key c++-mode-map (kbd "; a ,")
  (lambda ()
    (interactive)
    (insert (format "<>")) (backward-char 1)))

(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)

(require 'config-keybindings nil t)
;; (require 'config-misc nil t)
(require 'config-site nil t) ; Site-specific configuration.
(require 'config-host nil t) ; Host-specific configuration.

(defun make-launcher-frame ()
  (interactive)
  (with-selected-frame
      (make-frame '((name . "Launcher.Emacs")
                    (modeline . nil)
                    ;; (minibuffer . only)
                    (fullscreen . 0)
                    (undecorated . t)
                    (alpha . 95)))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; -*- lexical-binding: t; -*-

(deftheme plain-light
  "A plain light theme.")

(require 'lib-color)

(pcase-let*
    ((`(,c0 ,h0 ,h1 ,h2 ,h3 ,h4 ,h5)
      (random-element
       `(;; 0 0 54 ...
         ;; 0 0 120 ...
         (0 0 120 60 180 210 10)
         (0 0 195 270 180 210 10)
         (0 0 290 340 180 210 10))))

     (h.red 12)
     (h.orange 40)
     (h.yellow 86)
     (h.grass 120)
     (h.green 127)
     (h.blue 240)
     (h.cyan 231)
     (h.purple 308)
     (h.pink 350)

     (bg                     (color->hex 100 0 0))
     (fg                     (color->hex 23 c0 h0))
     (color.keyword          (color->hex 44 100 h1))
     (color.builtin          (color->hex 33 100 h2))
     (color.highlight        (color->hex 40 120 h4))
     (color.border           (color->hex 50 0 0)))

  (color-make-theme
   'plain-light
   `((default                       ,fg ,bg)
     (cursor                        () (60 70 ,h1))
     (font-lock-bracket-face        (65 15 ,h1))
     (font-lock-builtin-face        ,color.builtin)
     (font-lock-comment-face        (70 0 0) () :italic t)
     (font-lock-comment-delimiter-face (85 0 0) () :italic t)
     (font-lock-constant-face       ,color.builtin)
     (font-lock-doc-face            (50 20 ,h1) (95 0 0))
     (font-lock-function-name-face  ,fg () :weight bold)
     (font-lock-keyword-face        ,color.keyword)
     (font-lock-preprocessor-face   ,color.keyword)
     (font-lock-string-face         (45 0 0) (95 0 0))
     (font-lock-type-face           ,color.builtin)
     (font-lock-variable-name-face  (35 0 0) () :weight bold)
     (font-lock-warning-face        (50 100 ,h5) () :weight bold)
     (dired-directory               (30 100 ,h.blue))
     (dired-symlink                 (30 100 ,h.purple))
     (dired-ignored                 (60 0 0))
     (dired-marked                  (30 100 ,h.green) (100 40 ,h.green)
                                    :weight bold)
     (dired-flagged                 (30 170 ,h.red) (85 85 ,h.red)
                                    :weight bold)
     (dired-perm-write              ,color.builtin () :weight bold)
     (ediff-even-diff-A             () (90 0 0))
     (ediff-even-diff-B             () (90 0 0))
     (ediff-odd-diff-A              () (95 0 0))
     (ediff-odd-diff-B              () (95 0 0))
     (ediff-current-diff-A          () (84 25 12))
     (ediff-current-diff-B          () (84 25 127))
     (ediff-fine-diff-A             () (71 35 12))
     (ediff-fine-diff-B             () (71 35 127) )
     (ffap                          ,color.highlight)
     (fringe                        (90 0 0) ,bg)
     (header-line                   (60 0 0) (90 0 0))
     (highlight                     ,color.highlight (90 0 0))
     (holiday-face                  (57 0 0) ,fg)
     (isearch                       ,fg (85 40 ,h4) :weight normal :underline t)
     (lazy-highlight                ,fg (95 15 ,h4))
     (minibuffer-prompt             ,color.keyword)
     (mode-line                     (40 0 0) ,bg
                                    :box ,(list :line-width 2 :color bg)
                                    :overline ,color.border)
     (mode-line-inactive            (70 0 0) ,bg
                                    :box ,(list :line-width 2 :color bg)
                                    :overline ,color.border)
     (mode-line-highlight           ,color.highlight (90 30 ,h4))
     (mode-line-buffer-id           unspecified () :weight semi-bold)
     (mode-line-mousable            (47 100 50) ,bg)
     (mode-line-mousable-minor-mode (60 0 0) ,bg)
     (outline-1                     (30 130 ,h1))
     (outline-2                     (40 100 ,h1))
     (outline-3                     (50 80 ,h1))
     (outline-4                     (60 60 ,h1))
     (org-document-info             (40 100 ,h2))
     (org-document-info-keyword     (70 40 ,h2))
     (org-document-title            (30 130 ,h2))
     (paren-face                    (65 15 ,h1))
     (region                        () (95 20 ,h1))
     (scroll-bar                    () (90 0 0))
     (show-paren-match              (40 80 ,h1) (90 20 ,h1) :weight bold)
     (show-paren-mismatch           ,bg (50 150 ,h5) :weight bold)
     (tooltip                       (57 0 0) (40 0 0))
     (vertical-border               (75 0 0))
     (widget-button-face            (60 0 0) () :weight bold)
     (widget-field-face             (70 0 0) () :weight bold))))

(provide-theme 'plain-light)

;; Local Variables:
;; no-byte-compile: t
;; End:

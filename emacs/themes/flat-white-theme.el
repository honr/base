;; -*- lexical-binding: t; -*-

(deftheme flat-white
  "A serious looking black on white theme.")

(let ((bold 'medium)

      (color-main-soft "#905")
      (color-main-dark "#603")

      (color-red "Red")

      (color-white "#ffffff")
      (color-grey-e "#eeeeee")
      (color-grey-d "#dddddd")
      (color-grey-c "#cccccc")
      (color-grey-b "#bbbbbb")
      (color-grey-a "#aaaaaa")
      (color-grey-9 "#999999")
      (color-grey-8 "#888888")
      (color-grey-7 "#777777")
      (color-grey-6 "#666666")
      (color-grey-5 "#555555")
      (color-grey-4 "#444444")
      (color-grey-3 "#333333")
      (color-grey-2 "#222222")
      (color-grey-1 "#111111")
      (color-black  "#000000")

      ;; Misc colors:
      ;;   "#0088ff"
      ;;   "#00bb00"
      ;;   "#06f"
      ;;   "#080"
      ;;   "#440"
      ;;   "#66cc33"
      ;;   "#808"
      ;;   "#990000"
      ;;   "#aaccff"
      ;;   "#bbffbb"
      ;;   "#d03"
      ;;   "#ff99bb"
      ;;   "#ffbb00"
      ;;   "#ffdd66"
      ;;   "light blue"
      )

  (custom-theme-set-faces
   `flat-white
   `(default                       ((t (:foreground ,color-grey-2 :background ,color-white))))
   `(cursor                        ((t (:background "#66cc33"))))
   `(font-lock-builtin-face        ((t (:foreground ,color-main-soft))))
   `(font-lock-comment-face        ((t (:foreground ,color-grey-9 :italic t))))
   `(font-lock-constant-face       ((t (:foreground ,color-main-soft))))
   `(font-lock-doc-face            ((t (:foreground ,color-main-soft :background ,color-grey-e))))
   `(font-lock-function-name-face  ((t (:foreground ,color-black :weight ,bold))))
   `(font-lock-keyword-face        ((t (:foreground ,color-main-dark))))
   `(font-lock-preprocessor-face   ((t (:foreground ,color-main-dark))))
   `(font-lock-string-face         ((t (:foreground ,color-grey-6 :background ,color-grey-e))))
   `(font-lock-type-face           ((t (:foreground ,color-main-soft))))
   `(font-lock-variable-name-face  ((t (:foreground ,color-grey-2 :weight ,bold))))
   `(font-lock-warning-face        ((t (:foreground "#d03" :weight ,bold))))

   `(dired-directory               ((t (:foreground "#06f"))))
   `(dired-symlink                 ((t (:foreground "#808"))))
   `(dired-ignored                 ((t (:foreground ,color-grey-8))))
   `(dired-marked                  ((t (:foreground "#080" :weight ,bold))))
   `(dired-flagged                 ((t (:foreground ,color-white :background "#990000" :weight ,bold))))
   `(dired-perm-write              ((t (:foreground ,color-main-soft :weight ,bold))))
   `(scroll-bar                    ((t (:background ,color-grey-d))))
   `(fringe                        ((t (:foreground ,color-grey-b :background ,color-white))))
   `(header-line                   ((t (:foreground ,color-grey-8 :background ,color-grey-d))))
   `(highlight                     ((t (:foreground "light blue" :background ,color-grey-d))))
   ;; (highline-face               ((t (:background "SeaGreen")))) ;;
   `(holiday-face                  ((t (:foreground ,color-grey-7 :background ,color-black))))
   `(isearch                       ((t (:foreground ,color-white :background "#ffbb00"))))
   `(isearch-lazy-highlight-face   ((t (:foreground ,color-white :background "#ff99bb"))))
   ;; (isearch-secondary           ((t (:foreground "green"))))
   `(menu                          ((t (:foreground ,color-white :background ,color-grey-b))))
   `(minibuffer-prompt             ((t (:foreground ,color-grey-5))))
   `(mode-line                     ((t (:foreground ,color-grey-6 :background ,color-grey-d :box (:line-width 1 :color ,color-grey-b)))))
   `(mode-line-inactive            ((t (:foreground ,color-grey-9 :background ,color-grey-e :box (:line-width 1 :color ,color-grey-b)))))
   `(mode-line-buffer-id           ((t (:foreground "#440" :weight ,bold))))
   `(mode-line-mousable            ((t (:foreground ,color-grey-5 :background ,color-black))))
   `(mode-line-mousable-minor-mode ((t (:foreground ,color-grey-8 :background ,color-white))))
   `(region                        ((t (:background "#ffdd66"))))
   `(secondary-selection           ((t (:foreground "#0088ff" :background "#aaccff"))))
   `(show-paren-match-face         ((t (:foreground "#00bb00" :background "#bbffbb"))))
   `(show-paren-mismatch-face      ((t (:foreground ,color-white :background ,color-red))))
   `(paren-face                    ((t (:foreground ,color-grey-b))))
   `(tool-bar                      ((t (:foreground ,color-grey-7 :background ,color-grey-1))))
   `(tooltip                       ((t (:foreground ,color-grey-7 :background ,color-grey-3))))
   `(widget-button-face            ((t (:foreground ,color-grey-8 :weight ,bold))))
   `(widget-field-face             ((t (:foreground ,color-grey-9 :weight ,bold))))))

(provide-theme 'flat-white)

;; Local Variables:
;; no-byte-compile: t
;; End:

(defun compilation-exit-bury-on-success (status code msg)
  (when (and (eq status 'exit) (zerop code))
    (bury-buffer "*compilation*")
    (replace-buffer-in-windows "*compilation*"))
  (cons msg code))
(setq compilation-exit-message-function 'compilation-exit-bury-on-success)

(defun wrap-lines-in-quotes (beg end &optional prefixarg)
  "Wraps lines in quotes while quoting the string itself.  Uses
regular emacs-lisp prin1 for quoting.  With a prefix arg, places
prefix spaces into the quotes."
  (interactive "*r\np")
  (let ((re (if (zerop (logand 1 prefixarg)) "^\\(.*\\)$" "^\s*\\(.*\\)$"))
        (line-prefix (if (zerop (logand 2 prefixarg)) "" "  "))
        (line-trail (if (zerop (logand 4 prefixarg)) "" "\\n")))
    (save-excursion
      (save-restriction
        (narrow-to-region beg end)
        (goto-char (point-min))
        (while (search-forward-regexp re)
          (replace-match
           (concat "\"" line-prefix
                   (substring (prin1-to-string
                               (match-string-no-properties 1)) 1 -1)
                   line-trail "\"")
           t t nil 1))))))
(global-set-key (kbd "C-M-\"") 'wrap-lines-in-quotes)

;; (global-hl-line-mode 0)
;; ;; (fset 'yes-or-no-p 'y-or-n-p)

;; Better scrolling
;; (setq scroll-margin 0
;;       scroll-conservatively 100000
;;       scroll-preserve-screen-position 1)

;; (to-shortdate "2022-05-10") -> esij
(defun to-shortdate (date)
  (let* ((parsed-date (parse-time-string date))
         (day (nth 3 parsed-date))
         (month (nth 4 parsed-date))
         (year (nth 5 parsed-date)))
    (format "%c%c%c%c"
            (+ 97 (/ (- year 1900) 26))
            (+ 97 (% (- year 1900) 26))
            (+ 97 (* 2 (- month 1)) (/ (- day 1) 16))
            (+ 97 (% (- day 1) 16)))))

;; (from-shortdate "esij") -> 2022-05-10
(defun from-shortdate (sdate)
  (let* ((year (+ 1900 (* 26 (- (aref sdate 0) 97)) (- (aref sdate 1) 97)))
         (m (- (aref sdate 2) 97))
         (d (- (aref sdate 3) 97))
         (month (+ 1 (/ m 2)))
         (day (+ 1 (* 16 (% m 2)) d)))
    (format-time-string "%Y-%m-%d"
                        (encode-time (list 0 0 0 day month year nil 0 nil)))))

(setq gdb-many-windows t)
(setq gdb-show-main t)

(require 'sh-script nil t)
(require 'sh-skeletons nil t)

(defun mutate-region (f)
  (interactive "aFunction to apply to region: ")
  (save-excursion
    (save-restriction
      (narrow-to-region (region-beginning) (region-end))
      (let ((replacement-text (funcall f (buffer-string))))
        (delete-region (point-min) (point-max))
        (insert replacement-text)))))

(defun tabulize-with-percentage (input-string)
  "Make an org table out of something like \"foo 0.4\\nbar 0.6\""
  (let* ((raw-table (split-string input-string "\n" t))
         (table (mapcar (lambda (kv-str)
                          (let* ((kv (split-string kv-str))
                                 (k (first kv))
                                 (v (string-to-number (second kv))))
                            (list k v)))
                        raw-table))
         (values (mapcar 'second table))
         (sum (apply '+ 0.0 values))
         (max-key-length (apply 'max (length "TOTAL")
                                (mapcar (lambda (kv) (length (first kv)))
                                        table)))
         (fmt-string (format "| %%-%ds | %%8.4f | %%5.1f%%%% |"
                             max-key-length)))
    (concat
     (mapconcat (lambda (kv)
                  (let ((k (first kv))
                        (v (second kv)))
                    (format fmt-string
                            k v (* 100.0 (/ v sum))))) table "\n")
     "\n"
     (format fmt-string "TOTAL" sum 100.0))))

(defun css-matrix (radius angle-degrees x0 y0)
  (let* ((r (* angle-degrees (/ pi 180)))
         (c (* radius (cos r)))
         (s (* radius (sin r)))
         (x1 (+ (* c x0) (* -1 s y0)))
         (y1 (+ (* s x0) (* c y0))))
    (format "matrix(%.1f,%.1f,%.1f,%.1f,%.1f,%.1f)"
            c (- s) s c (- x1) (- y1))))

;; (css-matrix 1 60 0 0)

(defun fill-to-line ()
  (interactive)
  (let ((fill-column (point-max)))
    (fill-paragraph nil)))

(provide 'config-misc)

(modify-syntax-entry ?. "w" sh-mode-syntax-table)

(define-abbrev sh-mode-abbrev-table ".be" "" 'sh-skeletons-be)
(define-abbrev sh-mode-abbrev-table ".verb" "" 'sh-skeletons-verb)

(define-skeleton sh-skeletons-be
  "be" nil
  "#!/bin/bash

declare -A docs=()
docs[verb.help]=\"<No or one arg>
  With no args, prints list of verbs.
  With one arg, considered the verb, prints the docstring for that verb.\"
verb.help() {
  if [[ \"$#\" = 0 ]] ; then
    verb.
  elif [[ \"$#\" = 1 ]] ; then
    local program=\"${0##*/}\"
    local verb=\"$1\"
    echo \"\\$ ${program} ${verb} ${docs[verb.$verb]}\"
  fi
}

verb.() {
  local program=\"${0##*/}\"
  echo \"Usage:\"
  # Exposed verb functions must start with \"verb.\", e.g., verb.build-all().
  declare -F | while read f ; do  # f looks like \"declare -f Functionname\"
    if [[ \"$f\" =~ ^declare\\ -f\\ [verb.] ]] ; then
      local verb=\"${f#declare -f verb.}\"  # Strip the prefix.
      local docstring=\"${docs[verb.$verb]}\"
      echo \"\\$ ${program} ${verb} ${docstring}\"
    fi
  done
}" ?\n ?\n

  ".verb" _ ?\n ?\n

  "\"verb.$@\""
  )

(define-skeleton sh-skeletons-verb
  "verb" nil
  '(setq v1 (read-string "Verb: "))
  "docs[verb." v1 "]=\"ARGS\"" ?\n
  "verb." v1 "() {" ?\n
  > _ ?\n
  > "set -x" ?\n
  "}" ?\n)

(provide 'sh-skeletons)

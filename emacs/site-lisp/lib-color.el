;; -*- lexical-binding: t; -*-

(require 'cl-lib)
(require 'lib-sexp)

(setq color--consts `((sRGB ((0.4124564  0.3575761  0.1804375)
                            (0.2126729  0.7151522  0.0721750)
                            (0.0193339  0.1191920  0.9503041))
                           ((3.2404542 -1.5371385 -0.4985314)
                            (-0.9692660  1.8760108  0.0415560)
                            (0.0556434 -0.2040259  1.0572252)))
                     (AppleRGB ((0.4497288  0.3162486  0.1844926)
                                (0.2446525  0.6720283  0.0833192)
                                (0.0251848  0.1411824  0.9224628))
                               ((2.9515373 -1.2894116 -0.4738445)
                                (-1.0851093  1.9908566  0.0372026)
                                (0.0854934 -0.2694964  1.0912975)))
                     (kappa . ,(/ (* 29 29 29) (* 3.0 3 3)))
                     (kappa-epsilon . 8.0)
                     (epsilon . ,(/ (* 8.0 3 3 3) (* 29 29 29)))
                     ;; (D65 . (0.953 1.000 1.086)) ;; t32
                     (D65 . (0.95047 1.00000 1.08883))))

(defun color--const (key)
  (cdr (assoc key color--consts)))

(defun color--f1 (y_ratio)
  (if (< (color--const 'epsilon) y_ratio)
      (- (* 116.0 (expt y_ratio (/ 1.0 3))) 16.0)
    (* (color--const 'kappa) y_ratio)))

(defun color--f1-inv (l)
  (if (> l (color--const 'kappa-epsilon))
      (expt (/ (+ l 16) 116.0) 3.0)
    (/ l (color--const 'kappa))))

(defun color--xyz-to-uv (x y z)
  (let ((denom (+ x (* 15.0 y) (* 3.0 z))))
    (if (zerop denom)
        (list 0.0 0.0)
      (list (/ (* 4.0 x) denom) (/ (* 9.0 y) denom)))))

(defun color->lch (c)
  (let* ((rgb (mapcar
               (lambda (hex-substring)
                 (let ((V (/ (string-to-number hex-substring 16) 255.0)))
                   ;; sRGB companding
                   (if (<= V 0.04045)
                       (/ V 12.92)
                     (expt (/ (+ V 0.055) (+ 1 0.055)) 2.4))))
               (list (substring c 1 3)
                     (substring c 3 5)
                     (substring c 5 7))))
         (x_0 (car (color--const 'D65)))
         (y_0 (cadr (color--const 'D65)))
         (z_0 (caddr (color--const 'D65)))
         (uv_0 (color--xyz-to-uv x_0 y_0 z_0))

         (sRGB->xyz-matrix (car (color--const 'sRGB)))
         (xyz (mapcar (lambda (row) (apply #'+ (cl-mapcar '* row rgb)))
                      sRGB->xyz-matrix))
         (x (car xyz))
         (y (cadr xyz))
         (z (caddr xyz))
         (uv_1 (color--xyz-to-uv x y z))

         (l (color--f1 (/ y y_0)))
         (u (* 13.0 l (- (car uv_1) (car uv_0))))
         (v (* 13.0 l (- (cadr uv_1) (cadr uv_0)))))
    (list l
          (sqrt (+ (* u u) (* v v)))
          (mod (* (/ 180 float-pi) (atan v u)) 360))))

(defun color->hex (&rest args)
  (let* ((lch (cond
               ((listp (car args)) (car args))
               ((vectorp (car args)) (mapcar #'identity (car args)))
               ((numberp (car args)) args)
               ((stringp (car args)) nil ;; TODO: pass the number right through.
                )))
         (l (car lch))
         (chroma (cadr lch))
         (h-radians (* (/ float-pi 180) (caddr lch)))

         (u (* chroma (cos h-radians)))
         (v (* chroma (sin h-radians)))

         (x_0 (car (color--const 'D65)))
         (y_0 (cadr (color--const 'D65)))
         (z_0 (caddr (color--const 'D65)))
         (uv_0 (color--xyz-to-uv x_0 y_0 z_0))
         (u_0 (car uv_0))
         (v_0 (cadr uv_0))

         (y (* y_0 (color--f1-inv l)))
         (a (* (/ 1.0 3)
               (if (< -0.0001 u 0.0001)
                   (/ (- 4.0 u_0) u_0)
                 (/ (- (* l (- 4.0 u_0))
                       (* (/ 1 13.0) u))
                    (+ (* l u_0)
                       (* (/ 1 13.0) u))))))
         (b (* -5.0 y))
         (c (/ -1 3.0))
         (d (* 5.0 y
               (if (< -0.0001 v 0.0001)
                   (/ (- (/ 3.0 5.0) v_0) v_0)
                 (/ (- (* l (- (/ 3.0 5.0) v_0))
                       (* (/ 1 13.0) v))
                    (+ (* l v_0)
                       (* (/ 1 13.0) v))))))
         (x (/ (- d b) (- a c)))
         (z (+ (* x a) b))
         (xyz (list x y z))
         (RGB (mapcar (lambda (v)
                        (let ((V (if (<= v 0.0031308) ;; sRGB companding
                                     (* 12.92 v)
                                   (- (* (+ 1 0.055) (expt v (/ 1 2.4)))
                                      0.055))))
                          (cond ((<= V 0) 0)
                                ((<= 1 V) 255)
                                (t (round (* 255.0 V))))))
                      (mapcar (lambda (row) (apply #'+ (cl-mapcar '* row xyz)))
                              (cadr (color--const 'sRGB))))))
    (format "#%02X%02X%02X" (car RGB) (cadr RGB) (caddr RGB))))

;; (color->hex [10 20 30]) "#321200"
;; (color->hex 10 20 30) "#321200"
;; (color->hex (list 10 20 30)) "#321200"
;; (color->hex "#321200") -> currently an error.

;; All over place, so only useful for testing purposes.
;; TODO: make it uniform over lch fields.
(defun color-random ()
  (format "#%02X%02X%02X"
          (/ (random 65536) 257)
          (/ (random 65536) 257)
          (/ (random 65536) 257)))

;; ;; Test
;; (dotimes (i 20)
;;   (let* ((c (color-random))
;;          (lch (color->lch c)))
;;     (insert (format "%3s (%s = %s) [%.2f %.2f %.2f]\n"
;;                     (equal c (color->hex lch)) c (color->hex lch)
;;                     (car lch) (cadr lch) (caddr lch)))))

(defun color-make-theme (theme-name make-theme-specs)
  (cl-flet ((shorthand-color (spec)
              (cond
               ((stringp spec) spec)
               ((symbolp spec) spec)
               ((listp spec) (color->hex spec))
               (t spec))))
   (apply 'custom-theme-set-faces
          theme-name
          (mapcar
           (lambda (row)
             (let ((face-name (car row))
                   (fg-spec (cadr row))
                   (bg-spec (caddr row))
                   (other-attributes (cdddr row)))
               (list face-name
                     (list
                      (list '((class color) (min-colors 16777216)) ;; 256^3.
                            (append
                             (when fg-spec
                               (list :foreground (shorthand-color fg-spec)))
                             (when bg-spec
                               (list :background (shorthand-color bg-spec)))
                             other-attributes))))))
           make-theme-specs))))

;; TODO: define color-make-theme-from-rules.  It would take a set of rules,
;; slightly similar to CSS, to come up with the final theme.
;; Types of rules:
;; - $v {fg: red, bg: white} => define a temporary style.
;; - x, y: {fg: z.fg}
;; - x: {fg: v1, bg: v2}
;; - x: {fg: w1 v1 + w2 v2}
;; - x: {w x1 + (1-w) bg}
;; [[l_fg, c_fg, h_fg], [l_bg, c_bg, h_bg]], <-- colors
;; <(EL|L|SL|R|SB|B|EB), (SIR), (fgNil|fgT|noFg|FgR), (bgNil|bgT|noBg|BgR),
;;  Box:..., Underline: ..., Overline: ...> <-- properties
;;
;; Color operations:
;;
;; - given color B and color C and weight w, find the weighted middle point
;;   (w C + (1-w) B).
;; - Represent {t, nil, and Missing} as special colors.  (Inf, -Inf, NaN)?  How
;;   about bg and fg? (or max and min, if bg and fg are not those?)

(provide 'lib-color)

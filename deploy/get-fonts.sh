#!/bin/bash

fonts=(
  # Monospace fonts:
  "B612 Mono"
  "Courier Prime"
  "DM Mono"
  # "Fira Code"
  "IBM Plex Mono"
  # "Inconsolata"
  "Nova Mono"
  "Oxygen Mono"
  "PT Mono"
  # "Source Code Mono"
  "Spline Sans Mono"
  "Ubuntu Mono"

  # Other fonts
  "Assistant"
  # "Fira Sans"
  # "Fira Sans Condensed"
  # "Fira Sans Extra Condensed"
  "Gelasio"
  # "Georama"
  "Lato"
  "Merriweather"
  "Merriweather Sans"
  # "Noto Sans"
  # "Noto Serif"
  "Pontano Sans"
  "Rosario"
  "Source Sans 3"
  "Varta"
  "Yanone Kaffeesatz"
)

for font in "${fonts[@]}"; do
  obtain-font "$font"
done

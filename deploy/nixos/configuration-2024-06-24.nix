# See configuration.nix(5) or the NixOS manual by running nixos-help.

{ config, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  nix.nixPath = [ "nixpkgs=/nix/src:nixos-config=/etc/nixos/configuration.nix:/nix/src" ];

  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
    # grub = {
    #   enable = true;
    #   efiSupport = true;
    #   useOSProber = true;
    #   device = "nodev";
    # };
  };
  boot.kernelPackages = pkgs.linuxPackages_latest;
  networking.hostName = "rey";
  networking.networkmanager.enable = true;
  networking.wireguard.enable = true;
  # Firewall:
  networking.firewall.allowedTCPPorts = [ 8000 ]; ####
  # networking.firewall.allowedUDPPorts = [...];
  # networking.firewall.enable = false; # Disable firewall.
  # hardware.opengl.driSupport32Bit = true;
  hardware.graphics.enable32Bit = true;
  # hardware.opengl.driSupport = true;
  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
    support32Bit = true;
  };
  hardware.bluetooth.enable = true;
  i18n = {
    defaultLocale = "en_US.UTF-8";
  };
  console.font = "Lat2-Terminus16";
  console.keyMap = "us";
  time.timeZone = "###################";
  nixpkgs.config.allowUnfree = true; # CUPS needs this.

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    alacritty
    alsaUtils
    aspell
    aspellDicts.en
    bash
    clang-manpages
    clang_16
    curl
    # deno
    dict
    dunst
    (emacs.override {withGTK2 = false; withGTK3 = false;})
    file
    firefox
    git
    go
    google-chrome
    google-fonts
    i3
    ispell
    iw
    jq
    man-pages # Also see https://nixos.wiki/wiki/Man_pages
    # pandoc
    pavucontrol
    rofi
    rsync
    rxvt-unicode-unwrapped
    screen
    # tmux
    unzip
    vanilla-dmz
    vim
    whois
    vivaldi
    vivaldi-ffmpeg-codecs
    widevine-cdm
    xterm
    zip
    zsh
  ];

  fonts = {
    enableDefaultPackages = true;
    packages = [
      pkgs.ibm-plex
      pkgs.merriweather
      pkgs.merriweather-sans
      pkgs.open-sans
      pkgs.paratype-pt-mono
      pkgs.paratype-pt-sans
      pkgs.paratype-pt-serif
      pkgs.roboto
      pkgs.vazir-fonts
    ];
    fontconfig = {
      defaultFonts = {
        serif = ["Vazir" "Merriweather" ];
        sansSerif = ["Roboto"];
        monospace = ["IBMPlex"];
      };
    };
  };

  # Some programs need SUID wrappers, can be configured further or are started
  # in user sessions.
  # programs.bash.enableCompletion = true;
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    # Seems broken.  Turned off in favour of programs.ssh.startAgent.
    # enableSSHSupport = true;
  };
  programs.ssh.startAgent = true;
  programs.zsh.enable = true;

  # programs.steam = {
  #   enable = true;
  #   # remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
  #   # dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  # };

  services.openssh.enable = true; # The OpenSSH daemon

  hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.beta;

  # Comment out to disable XServer in preparation to try wayland.
  services.xserver = {
    enable = true;
    xkb = {
      options = "ctrl:nocaps,altwin:swap_alt_win";
      layout = "us";
    };
    videoDrivers = ["nvidia"];
    # videoDrivers = ["nouveau"];
    windowManager.i3.enable = true;
  };
  services.libinput = {
    enable = true;
    mouse.naturalScrolling = true;
  };
  services.displayManager.sddm = {
    enable = true;
    enableHidpi = true;
  };
  services.displayManager.autoLogin = {
    enable = true;
    user = "####";
  };
  services.displayManager.defaultSession = "none+i3";

  systemd.user.services."dunst" = {
    enable = true;
    description = "Notification daemon";
    wantedBy = [ "default.target" ];
    serviceConfig.Restart = "always";
    serviceConfig.RestartSec = 2;
    serviceConfig.ExecStart = "${pkgs.dunst}/bin/dunst";
  };

  # programs.sway.enable = true;

  sound.enable = true;

  services.printing.enable = true;
  services.printing.drivers = [ pkgs.brlaser pkgs.hll2390dw-cups ];
  services.avahi.enable = true;
  services.upower.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.foo = { ####
    isNormalUser = true;
    uid = 2000;
    extraGroups = ["networkmanager" "wheel" "sound" "audio" "video"];
    shell = pkgs.zsh;
  };

  users.extraUsers.bar = {
    isNormalUser = true;
    uid = 2001;
    extraGroups = ["networkmanager"];
    shell = pkgs.zsh;
  };

  services.udev = {
    extraRules = ''
      KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="1050", ATTRS{idProduct}=="0113|0114|0115|0116|0120|0200|0402|0403|0406|0407|0410", TAG+="uaccess"
      '';
  };

  system.stateVersion = "22.11";
}

# Cleanup gc roots:
# $ rm /nix/var/nix/gcroots/auto/*
# $ nix-collect-garbage -d

import os
import ptpython, prompt_toolkit
from prompt_toolkit.formatted_text import HTML

ZOO_DIR=os.path.expanduser('~/Zoo')

def configure(repl: ptpython.repl.PythonRepl):

  class CustomPrompt(ptpython.prompt_style.PromptStyle):
    def in_prompt(self):
      i = repl.current_statement_index
      return HTML(f"<ansigreen>{i}.</ansigreen> ")

    def in2_prompt(self, width):
      gap = ''.rjust(width - 2)
      return HTML(f"<ansigreen>{gap}:</ansigreen> ")

    def out_prompt(self):
      return HTML(" ↳ ")


  repl.all_prompt_styles["custom"] = CustomPrompt()
  repl.prompt_style = "custom"
  repl.use_code_colorscheme("material") # or default for light background
  repl.color_depth = "DEPTH_8_BIT"  # 256 colors; DEPTH_24_BIT: True color.
  repl.min_brightness = 0.0  # Increase for dark terminal backgrounds.
  repl.max_brightness = 1.0  # Decrease for light terminal backgrounds.
  repl.enable_syntax_highlighting = True
  repl.show_signature = False  # TODO: Should show in the toolbar.
  repl.show_docstring = True
  repl.show_meta_enter_message = True
  toolbar = ptpython.layout.CompletionVisualisation.TOOLBAR
  repl.completion_visualisation = toolbar
  repl.completion_menu_scroll_offset = 0
  repl.enable_system_bindings = True  # Enables Control-Z suspend.
  repl.show_line_numbers = False
  repl.show_status_bar = True
  repl.show_sidebar_help = True
  repl.swap_light_and_dark = False
  repl.highlight_matching_parenthesis = True
  repl.wrap_lines = True
  repl.enable_mouse_support = False
  repl.complete_while_typing = False
  repl.enable_fuzzy_completion = False
  repl.enable_dictionary_completion = False
  repl.vi_mode = False
  repl.paste_mode = False
  repl.insert_blank_line_after_output = False
  repl.enable_open_in_editor = True
  repl.confirm_exit = False
  repl.enable_input_validation = True
  repl.vi_start_in_navigation_mode = False
  repl.vi_keep_last_used_mode = False
  repl.enable_history_search = True  # TODO: Change the bindings to M-p and M-n.
  repl.enable_auto_suggest = True  # TODO: Cycle through suggestions.
  repl.title = '#' + os.getcwd().removeprefix(ZOO_DIR + '/') + ' '

ptpython.repl.embed(globals(),
                    locals(),
                    history_filename=os.path.join(os.getcwd(), 'history'),
                    configure=configure)

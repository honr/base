# It's not necessary to copy all keys to your config.
# If the key is missing in your config, "default-config.toml" will serve as a fallback

# You can use it to add commands that run after login to macOS user session.
# 'start-at-login' needs to be 'true' for 'after-login-command' to work
# Available commands: https://nikitabobko.github.io/AeroSpace/commands
after-login-command = []

# You can use it to add commands that run after AeroSpace startup.
# 'after-startup-command' is run after 'after-login-command'
# Available commands : https://nikitabobko.github.io/AeroSpace/commands
after-startup-command = []

# Start AeroSpace at login
start-at-login = false

# Normalizations. See: https://nikitabobko.github.io/AeroSpace/guide#normalization
enable-normalization-flatten-containers = true
enable-normalization-opposite-orientation-for-nested-containers = true

# See: https://nikitabobko.github.io/AeroSpace/guide#layouts
# The 'accordion-padding' specifies the size of accordion padding
# You can set 0 to disable the padding feature
accordion-padding = 30

# Possible values: tiles|accordion
default-root-container-layout = 'accordion'

# Possible values: horizontal|vertical|auto
# 'auto' means: wide monitor (anything wider than high) gets horizontal orientation,
#               tall monitor (anything higher than wide) gets vertical orientation
default-root-container-orientation = 'auto'

# Possible values: (qwerty|dvorak)
key-mapping.preset = 'qwerty'

# Gaps between windows (inner-*) and between monitor edges (outer-*).
# Possible values:
# - Constant:     gaps.outer.top = 8
# - Per monitor:  gaps.outer.top = [{ monitor.main = 16 }, { monitor."some-pattern" = 32 }, 24]
#                 In this example, 24 is a default value when there is no match.
#                 Monitor pattern is the same as for 'workspace-to-monitor-force-assignment'.
#                 See: https://nikitabobko.github.io/AeroSpace/guide#assign-workspaces-to-monitors
[gaps]
inner.horizontal = 0
inner.vertical =   0
outer.left =       0
outer.bottom =     0
outer.top =        0
outer.right =      0

# See https://nikitabobko.github.io/AeroSpace/guide#exec-env-vars
[exec]                   # Again, you don't need to copy all config sections to your config.
inherit-env-vars = true  # If you don't touch "exec" section,
[exec.env-vars]          # it will fallback to "default-config.toml"
# PATH = '/opt/homebrew/bin:/opt/homebrew/sbin:${PATH}'

# 'main' binding mode declaration
# See: https://nikitabobko.github.io/AeroSpace/guide#binding-modes
# 'main' binding mode must be always presented
[mode.main.binding]

# All possible keys:
# - Letters.        a, b, c, ..., z
# - Numbers.        0, 1, 2, ..., 9
# - Keypad numbers. keypad0, keypad1, keypad2, ..., keypad9
# - F-keys.         f1, f2, ..., f20
# - Special keys.   minus, equal, period, comma, slash, backslash, quote,
#                   semicolon, backtick, leftSquareBracket, rightSquareBracket,
#                   space, enter, esc, backspace, tab
# - Keypad special. keypadClear, keypadDecimalMark, keypadDivide, keypadEnter,
#                   keypadEqual, keypadMinus, keypadMultiply, keypadPlus
# - Arrows.         left, down, up, right
# All possible modifiers: cmd, alt, ctrl, shift
# All possible commands: https://nikitabobko.github.io/AeroSpace/commands

# See: https://nikitabobko.github.io/AeroSpace/commands#exec-and-forget
cmd-enter = 'exec-and-forget open /System/Applications/Utilities/Terminal.app'
cmd-shift-semicolon = 'exec-and-forget $HOME/Apps/Emacs.app/Contents/MacOS/bin/emacsclient -nc'

cmd-ctrl-1 = 'workspace 1'
cmd-ctrl-2 = 'workspace 2'
cmd-ctrl-3 = 'workspace 3'
cmd-ctrl-4 = 'workspace 4'
cmd-ctrl-5 = 'workspace 5'
cmd-ctrl-6 = 'workspace 6'
cmd-ctrl-7 = 'workspace 7'
cmd-ctrl-8 = 'workspace 8'
cmd-ctrl-9 = 'workspace 9'
cmd-ctrl-0 = 'workspace 10'

ctrl-shift-1 = 'move-node-to-workspace 1'
ctrl-shift-2 = 'move-node-to-workspace 2'
ctrl-shift-3 = 'move-node-to-workspace 3'
ctrl-shift-4 = 'move-node-to-workspace 4'
ctrl-shift-5 = 'move-node-to-workspace 5'
ctrl-shift-6 = 'move-node-to-workspace 6'
ctrl-shift-7 = 'move-node-to-workspace 7'
ctrl-shift-8 = 'move-node-to-workspace 8'
ctrl-shift-9 = 'move-node-to-workspace 9'
ctrl-shift-0 = 'move-node-to-workspace 10'

ctrl-backtick = 'workspace-back-and-forth'
# alt-shift-tab = 'move-workspace-to-monitor --wrap-around next'

ctrl-semicolon = 'mode arrange'

[mode.arrange.binding]
ctrl-semicolon = 'mode main'
esc = 'mode main'
ctrl-g = 'mode main'
enter = 'mode main'

ctrl-b = 'focus left'
ctrl-f = 'focus right'
ctrl-p = 'focus up'
ctrl-n = 'focus down'

ctrl-shift-b = 'move left'
ctrl-shift-f = 'move right'
ctrl-shift-p = 'move up'
ctrl-shift-n = 'move down'

alt-shift-b = 'join-with left'
alt-shift-f = 'join-with right'
alt-shift-n = 'join-with down'
alt-shift-p = 'join-with up'

slash = 'layout tiles horizontal vertical'
comma = 'layout accordion horizontal vertical'

minus = 'resize smart -50'
equal = 'resize smart +50'

shift-equal = 'fullscreen'

h = 'resize width -50'
j = 'resize height +50'
k = 'resize height -50'
l = 'resize width +50'

t = 'layout floating tiling'
e = 'layout tiles horizontal vertical'
z = 'layout h_accordion'
x = 'layout v_accordion'
c = 'layout h_tiles'
v = 'layout v_tiles'

ctrl-1 = 'workspace 1'
ctrl-2 = 'workspace 2'
ctrl-3 = 'workspace 3'
ctrl-4 = 'workspace 4'
ctrl-5 = 'workspace 5'
ctrl-6 = 'workspace 6'
ctrl-7 = 'workspace 7'
ctrl-8 = 'workspace 8'
ctrl-9 = 'workspace 9'
ctrl-0 = 'workspace 10'

ctrl-shift-1 = 'move-node-to-workspace 1'
ctrl-shift-2 = 'move-node-to-workspace 2'
ctrl-shift-3 = 'move-node-to-workspace 3'
ctrl-shift-4 = 'move-node-to-workspace 4'
ctrl-shift-5 = 'move-node-to-workspace 5'
ctrl-shift-6 = 'move-node-to-workspace 6'
ctrl-shift-7 = 'move-node-to-workspace 7'
ctrl-shift-8 = 'move-node-to-workspace 8'
ctrl-shift-9 = 'move-node-to-workspace 9'
ctrl-shift-0 = 'move-node-to-workspace 10'

r = 'reload-config'

cmd-b = ['focus left', 'mode main']
cmd-f = ['focus right', 'mode main']
cmd-p = ['focus up', 'mode main']
cmd-n = ['focus down', 'mode main']
cmd-shift-b = ['move left', 'mode main']
cmd-shift-f = ['move right', 'mode main']
cmd-shift-p = ['move up', 'mode main']
cmd-shift-n = ['move down', 'mode main']

cmd-r = ['reload-config', 'mode main']

cmd-u = ['flatten-workspace-tree', 'mode main'] # reset layout
cmd-k = ['close-all-windows-but-current', 'mode main']
